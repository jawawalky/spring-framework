/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2021 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.artemis;

import org.apache.activemq.artemis.core.config.Configuration;
import org.apache.activemq.artemis.core.config.impl.ConfigurationImpl;
import org.apache.activemq.artemis.core.server.embedded.EmbeddedActiveMQ;

import demo.util.Demo;

public class ArtemisServer {
	
	// constructors /////
	
	private ArtemisServer() { }
	
	
	// methods /////
	
	private void run() throws Exception {
		
		Demo.log("Starting Artemis MQ Server ...");
		
		Configuration config = new ConfigurationImpl();

		config.addAcceptorConfiguration("tcp", "tcp://127.0.0.1:61616");
		config.setSecurityEnabled(false);
		config.setPersistenceEnabled(false);
		
		EmbeddedActiveMQ server = new EmbeddedActiveMQ();
		server.setConfiguration(config);

		server.start();
		
		Demo.log("Server is running ...");
		Demo.input("Shut down server with Ctrl+C or with via IDE.");
				
	}

	public static void main(final String[] args) {
		
		try {
			
			new ArtemisServer().run();
			
		} // try
		catch (Exception e) {
			
			Demo.terminate(e, 1);
			
		} // catch

	}

}
