/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2006 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.aop.ex02;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import demo.util.Demo;

/**
 * This is a simple demo, which shows you how you can configure your
 * beans.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors
	// ........................................................................

	private Main() { }

	// methods
	// ........................................................................

	private void runDemo() {

		Demo.log("Running demo ...");

		ApplicationContext context
			= new ClassPathXmlApplicationContext("beans.xml");
	
		Calculator calculator = (Calculator) context.getBean("calculator");

		calculator.setValue1(2);
		calculator.setValue2(6);
		calculator.add();
		this.printResult(calculator);

		calculator.setValue1(7);
		calculator.setValue2(4);
		calculator.sub();
		this.printResult(calculator);

		calculator.setValue1(2);
		calculator.setValue2(4);
		calculator.mlt();
		this.printResult(calculator);

		calculator.setValue1(9);
		calculator.setValue2(3);
		calculator.div();
		this.printResult(calculator);

		Demo.log("Finished.");

	}

	private void printResult(Calculator calculator) {

		Demo.log(calculator.getValue1()
			+ " "
			+ calculator.getOperation()
			+ " "
			+ calculator.getValue2()
			+ " = "
			+ calculator.getResult());

	}

	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Main().runDemo();

	}

}
