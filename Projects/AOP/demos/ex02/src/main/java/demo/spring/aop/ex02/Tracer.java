/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.aop.ex02;

import java.lang.reflect.Method;

import org.springframework.aop.MethodBeforeAdvice;

import demo.util.Demo;

/**
 * A method tracer, which logs a method entry.
 * 
 * @author Franz Tost
 *
 */
public class Tracer implements MethodBeforeAdvice {

	// methods
	// ........................................................................

	public void before(Method method, Object[] parameters, Object target) throws Throwable {
		
		if (method.getName().startsWith("get")) {
			
			return;
			
		} // if
		
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("ENTERED: " + method.getName() + "(");
		
		for (int i = 0; i < parameters.length; i++) {
			
			stringBuilder.append(parameters[i].toString());
			stringBuilder.append((i < parameters.length - 1) ? ", " : "");
			
		} // for
		
		stringBuilder.append(")");
		
		Demo.log(stringBuilder.toString());
		
	}

}
