/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2006 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.aop.ex15;

import org.aopalliance.aop.Advice;
import org.springframework.aop.Advisor;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.aop.support.RegexpMethodPointcutAdvisor;

import demo.util.Demo;

/**
 * This is a simple demo, which shows you how you can configure your
 * beans.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors
	// ........................................................................

	private Main() { }

	// methods
	// ........................................................................

	private void runDemo() {

		Demo.log("Running demo ...");

		ProxyFactory factory = new ProxyFactory(new CalculatorImpl());
		
		Advice advice = new Tracer();
		Advisor advisor = new RegexpMethodPointcutAdvisor(".*mlt.*", advice);
		factory.addAdvisor(advisor);
		
		Calculator calculator = (Calculator) factory.getProxy();
		
		calculator.setA(2);
		calculator.setB(6);
		Demo.log("2 + 6 = " + calculator.add());

		calculator.setA(7);
		calculator.setB(4);
		Demo.log("7 - 4 = " + calculator.sub());

		calculator.setA(2);
		calculator.setB(4);
		Demo.log("2 * 4 = " + calculator.mlt());

		calculator.setA(9);
		calculator.setB(3);
		Demo.log("9 / 3 = " + calculator.div());

		Demo.log("Finished.");

	}

	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Main().runDemo();

	}

}
