/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.aop.ex12;

/**
 * The calculator.
 * 
 * @author Franz Tost
 *
 */
public class CalculatorImpl implements Calculator {
	
	// fields
	// ........................................................................
	
	private double a;
	private double b;
	
	// methods
	// ........................................................................
	
	public double getA()         { return this.a; }
	public void   setA(double a) { this.a = a;    }

	public double getB()         { return this.b; }
	public void   setB(double b) { this.b = b;    }

	public double add() { return this.a + this.b; }
	public double sub() { return this.a - this.b; }
	public double mlt() { return this.a * this.b; }
	public double div() { return this.a / this.b; }

}
