/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.aop.ex12;

import java.lang.reflect.Method;

import org.springframework.aop.ClassFilter;
import org.springframework.aop.MethodMatcher;
import org.springframework.aop.Pointcut;

/**
 * A generic <code>Pointcut</code> implementation, which is not evaluated at
 * runtime, since the <code>isRuntime()</code> method of the
 * <code>MethodMatcher</code> returns <code>false</code>.
 * 
 * @author Franz Tost
 *
 */
public class StaticPointcut implements Pointcut {
	
	// methods
	// ........................................................................

	@Override
	public ClassFilter getClassFilter() {
		
		return new ClassFilter() {

			@Override public boolean matches(final Class<?> type) {
				
				return Calculator.class.isAssignableFrom(type);
				
			}
			
		};
		
	}

	@Override
	public MethodMatcher getMethodMatcher() {
		
		return new MethodMatcher() {

			@Override public boolean isRuntime() { return false; }

			@Override public boolean matches(
				final Method   method,
				final Class<?> type
			) {
				
				String methodName = method.getName();
				return "add".equals(methodName) || "mlt".equals(methodName);
				
			}

			@Override public boolean matches(
				final Method    method,
				final Class<?>  type,
				final Object... args
			) {
				
				// This method is not evaluate for static matching
				//
				return false;
				
			}
			
		};
		
	}

}
