/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2006 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.aop.ex12;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import demo.util.Demo;

/**
 * This is a simple demo, which shows you how you can configure your
 * beans.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors
	// ........................................................................

	private Main() { }

	// methods
	// ........................................................................

	private void runDemo() {

		Demo.log("Running demo ...");

		try (
			ClassPathXmlApplicationContext context
				= new ClassPathXmlApplicationContext("beans.xml");
		) {

			Calculator calculator = (Calculator) context.getBean("calculator");
			
			calculator.setA(2);
			calculator.setB(6);
			Demo.log("2 + 6 = " + calculator.add());
	
			calculator.setA(7);
			calculator.setB(0);
			Demo.log("7 - 0 = " + calculator.sub());
	
			calculator.setA(0);
			calculator.setB(4);
			Demo.log("0 * 4 = " + calculator.mlt());
	
			calculator.setA(9);
			calculator.setB(3);
			Demo.log("9 / 3 = " + calculator.div());
	
			Demo.log("Finished.");
			
		} // try

	}

	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(final String[] args) {

		new Main().runDemo();

	}

}
