/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2021 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.aop.ex34;

import org.aopalliance.aop.Advice;
import org.springframework.aop.Advisor;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.aop.support.JdkRegexpMethodPointcut;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import demo.util.Demo;

/**
 * This is a simple demo, which shows you how you can configure your
 * beans.
 * 
 * @author Franz Tost
 *
 */
@Configuration
public class Application {

	// methods /////
	
	@Bean
	public Advisor advisor() {
		
		final JdkRegexpMethodPointcut pointcut = new JdkRegexpMethodPointcut();
		pointcut.setPattern(".*div.*");
		
		final Advice advice = new Tracer();
		
		return new DefaultPointcutAdvisor(pointcut, advice);
		
	} 

	@Bean
	public DefaultAdvisorAutoProxyCreator autoProxyCreator() {
		
		return new DefaultAdvisorAutoProxyCreator();
		
	}

	@Bean
	public Calculator calculator() {
		
		return new CalculatorImpl();
		
	} 

	private void runDemo(final ApplicationContext context) {

		Demo.log("Running demo ...");

		final Calculator calculator = context.getBean(Calculator.class);
		
		calculator.setValue1(2);
		calculator.setValue2(6);
		Demo.log("2 + 6 = " + calculator.add());

		calculator.setValue1(7);
		calculator.setValue2(4);
		Demo.log("7 - 4 = " + calculator.sub());

		calculator.setValue1(2);
		calculator.setValue2(4);
		Demo.log("2 * 4 = " + calculator.mlt());

		calculator.setValue1(9);
		calculator.setValue2(3);
		Demo.log("9 / 3 = " + calculator.div());

		Demo.log("Finished.");

	}

	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		try (
			AnnotationConfigApplicationContext context =
			    new AnnotationConfigApplicationContext(
			    	Application.class
			    );
		) {
			
			new Application().runDemo(context);
	
		} // try
			
	}

}
