/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2006 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.aop.ex10;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import demo.util.Demo;

/**
 * This is a simple demo, which shows you how you can configure your
 * beans.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors
	// ........................................................................

	/**
	 * A private default constructor.
	 */
	private Main() {

		super();

	}

	// methods
	// ........................................................................

	/**
	 * The demo.
	 */
	private void runDemo() {

		Demo.log("Running demo ...");

		ApplicationContext context
			= new ClassPathXmlApplicationContext("beans.xml");

		Calculator calculator = (Calculator) context.getBean("calculator");
		
		calculator.setValue1(2);
		calculator.setValue2(6);
		Demo.log("2 + 6 = " + calculator.add());

		calculator.setValue1(7);
		calculator.setValue2(4);
		Demo.log("7 - 4 = " + calculator.sub());

		calculator.setValue1(2);
		calculator.setValue2(4);
		Demo.log("2 * 4 = " + calculator.mlt());

		calculator.setValue1(9);
		calculator.setValue2(3);
		Demo.log("9 / 3 = " + calculator.div());

		Demo.log("Finished.");

	}

	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		Main app = new Main();
		app.runDemo();

	}

}
