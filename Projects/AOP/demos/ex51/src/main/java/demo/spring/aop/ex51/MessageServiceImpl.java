/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.aop.ex51;

import demo.util.Demo;

/**
 * The implementation of the message service.
 * 
 * @author Franz Tost
 *
 */
public class MessageServiceImpl implements MessageService {
	
	// fields
	// ........................................................................
	
	private String prefix;
	
	private String suffix;
	
	// methods
	// ........................................................................

	@Override public void setPrefix(final String prefix) {
		
		this.prefix = prefix;

	}

	@Override public void setSuffix(final String suffix) {
		
		this.suffix = suffix;

	}

	@Override public void service(final String message) {
		
		Demo.log(this.prefix + ":" + message + ":" + this.suffix);

	}

}
