/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2006 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.aop.ex51;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import demo.util.Demo;

/**
 * This is a simple demo, which shows you how you can configure your
 * beans.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors
	// ........................................................................

	private Main() { }

	// methods
	// ........................................................................

	private void runDemo() {

		Demo.log("Running demo ...");
		
		this.runAsPojo();
		this.runAsSpringBean();
		
		Demo.log("Finished.");

	}
	
	private void runAsPojo() {
		
		Demo.log("Running as POJO ...");
		
		MessageService service = new MessageServiceImpl();
		
		service.setPrefix("abc");
		service.setSuffix("xyz");
		
		service.service("POJO");
		
	}
	
	private void runAsSpringBean() {
		
		Demo.log("Running as Spring Bean ...");
		
		try (
			ClassPathXmlApplicationContext context =
			    new ClassPathXmlApplicationContext("beans.xml");
		) {
		
			MessageService service = (MessageService) context.getBean("service");
			
			service.setPrefix("abc");
			service.setSuffix("xyz");
			
			service.service("Spring Bean");
			
		} // try
				
	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Main().runDemo();

	}

}
