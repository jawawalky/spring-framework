/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex40;

import java.lang.reflect.Method;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

import demo.util.Demo;

/**
 * A simple profiler, which measures the execution time of a method.
 * 
 * @author Franz Tost
 * 
 */
public class Profiler implements MethodInterceptor {
	
	// methods
	// ........................................................................

	public Object invoke(MethodInvocation invocation) throws Throwable {
		
		long startTime = System.currentTimeMillis();
		
		try {
			
			return invocation.proceed();
			
		} // try
		finally {
			
			long endTime = System.currentTimeMillis();
			Method method = invocation.getMethod();
			Demo.log(method.getName() + "() took " + (endTime - startTime) + " ms.");
			
		} // finally
		
	}

}
