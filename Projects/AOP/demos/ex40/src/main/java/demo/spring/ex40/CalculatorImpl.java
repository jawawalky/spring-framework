/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex40;

import demo.util.Demo;

/**
 * @author Franz Tost
 *
 */
public class CalculatorImpl implements Calculator {
	
	// fields
	// ........................................................................
	
	private double value1;
	
	private double value2;
	
	// methods
	// ........................................................................
	
	public double getValue1() {
		
		return this.value1;
		
	}

	public void setValue1(double value1) {
		
		this.value1 = value1;
		
	}

	public double getValue2() {
		
		return this.value2;
		
	}

	public void setValue2(double value2) {
		
		this.value2 = value2;
		
	}

	public double add() {
		
		// Simulate long lasting action
		//
		Demo.sleep(200, 500);
		
		return this.value1 + this.value2;
		
	}

	public double sub() {
		
		// Simulate long lasting action
		//
		Demo.sleep(200, 500);
		
		return this.value1 - this.value2;
		
	}

	public double mlt() {
		
		// Simulate long lasting action
		//
		Demo.sleep(350, 750);
		
		return this.value1 * this.value2;
		
	}

	public double div() {
		
		// Simulate long lasting action
		//
		Demo.sleep(500, 1000);
		
		return this.value1 / this.value2;
		
	}

}
