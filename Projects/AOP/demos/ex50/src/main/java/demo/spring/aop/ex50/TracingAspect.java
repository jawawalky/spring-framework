/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.aop.ex50;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

import demo.util.Demo;

/**
 * A simple method tracer.
 * 
 * @author Franz Tost
 * 
 */
@Aspect
public class TracingAspect {
	
	// pointcuts
	// ........................................................................

	@Pointcut("execution(public * service(..))")
	public void serviceMethod() {};

	// advices
	// ........................................................................

	@Before("demo.spring.aop.ex50.TracingAspect.serviceMethod()")
	public void enter(final JoinPoint joinPoint) {
		
		Demo.log(joinPoint.toString());
		
	}

}
