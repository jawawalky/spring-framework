/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2021 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.aop.ex31;

import java.lang.reflect.Method;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

import demo.util.Demo;

/**
 * A method tracer, which logs the method entries and exits.
 * 
 * @author Franz Tost
 *
 */
public class Tracer implements MethodInterceptor {

	// methods
	// ........................................................................

	public Object invoke(MethodInvocation invocation) throws Throwable {
		
		try {
			
			Method method = invocation.getMethod();
			Demo.log("ENTERED: " + method.getName() + "()");
			
			return invocation.proceed();
			
		} // try
		finally {
			
			Method method = invocation.getMethod();
			Demo.log("EXITED: " + method.getName() + "()");
			
		} // finally
		
	}

}
