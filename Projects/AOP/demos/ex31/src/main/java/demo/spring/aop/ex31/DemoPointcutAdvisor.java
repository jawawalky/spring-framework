/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2021 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.aop.ex31;

import java.lang.reflect.Method;

import org.springframework.aop.support.StaticMethodMatcherPointcutAdvisor;

/**
 * A custom advisor, which defines join points for an advice. Only methods
 * which start with <i>mlt</i> or with <i>div</i> will be traced.
 * 
 * @author Franz Tost
 *
 */
public class DemoPointcutAdvisor extends StaticMethodMatcherPointcutAdvisor {

	private static final long serialVersionUID = 1L;
	
	// methods /////

	@Override
	public boolean matches(
		final Method   method,
		final Class<?> targetClass
	) {
		
		return
			method.getName().startsWith("mlt")
			||
			method.getName().startsWith("div");
		
	}

}
