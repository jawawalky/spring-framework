/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.aop.ex03;

import java.lang.reflect.Method;

import org.springframework.aop.MethodBeforeAdvice;

/**
 * An advice which counts method calls.
 * 
 * @author Franz Tost
 *
 */
public class InvocationCounter implements MethodBeforeAdvice {

	// fields
	// ........................................................................
	
	private int count;

	// methods
	// ........................................................................

	public void before(Method method, Object[] parameters, Object target) throws Throwable {
		
		this.count++;
		
	}
	
	/**
	 * Returns the number of method invocations.
	 * 
	 * @return The number of invocations.
	 */
	public int getCount() {
		
		return this.count;
		
	}

}
