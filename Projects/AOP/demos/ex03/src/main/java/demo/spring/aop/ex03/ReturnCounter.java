/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.aop.ex03;

import java.lang.reflect.Method;

import org.springframework.aop.AfterReturningAdvice;

/**
 * An advice which counts successful method returns.
 * 
 * @author Franz Tost
 *
 */
public class ReturnCounter implements AfterReturningAdvice {

	// fields
	// ........................................................................
	
	private int count;

	// methods
	// ........................................................................

	@Override
	public void afterReturning(Object returnValue, Method method, Object[] parameters, Object target) throws Throwable {
		
		this.count++;
		
	}

	/**
	 * Returns the number of regular method returns.
	 * 
	 * @return The number regular method calls.
	 */
	public int getCount() {
		
		return this.count;
		
	}

}
