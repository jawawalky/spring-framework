/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.aop.ex04;

import java.lang.reflect.Method;

import org.springframework.aop.MethodBeforeAdvice;

/**
 * An advice which counts method calls.
 * 
 * @author Franz Tost
 *
 */
public class InvocationCounter implements MethodBeforeAdvice {

	// fields
	// ........................................................................
	
	private int count;

	// methods
	// ........................................................................

	public void before(
		final Method   method,
		final Object[] parameters,
		final Object   target
	) throws Throwable {
		
		this.count++;
		
	}
	
	public int getCount() { return this.count; }

}
