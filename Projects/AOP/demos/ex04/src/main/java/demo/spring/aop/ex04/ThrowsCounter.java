/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.aop.ex04;

import java.lang.reflect.Method;

import org.springframework.aop.ThrowsAdvice;

/**
 * An advice which counts successful method returns.
 * 
 * @author Franz Tost
 *
 */
public class ThrowsCounter implements ThrowsAdvice {

	// fields
	// ........................................................................

	private int divisionByZeroExceptionCount;

	private int otherExceptionCount;

	// methods
	// ........................................................................

	public void afterThrowing(
		final Method    method,
		final Object[]  parameters,
		final Object    target,
		final Exception exception
	) throws Throwable {
		
		this.otherExceptionCount++;
		
	}

	public void afterThrowing(
		final Method method,
		final Object[] parameters,
		final Object target,
		final DivisionByZeroException exception
	) throws Throwable {
		
		this.divisionByZeroExceptionCount++;
		
	}

	public int getOtherExceptionCount() {

		return this.otherExceptionCount;

	}

	public int getDivisionByZeroExceptionCount() {

		return this.divisionByZeroExceptionCount;

	}

}
