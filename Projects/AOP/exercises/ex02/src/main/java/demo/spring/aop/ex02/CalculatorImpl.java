/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.aop.ex02;


/**
 * @author Franz Tost
 *
 */
public class CalculatorImpl implements Calculator {
	
	// fields
	// ........................................................................
	
	private double value1;
	
	private double value2;
	
	private double result;
	
	private String operation;
	
	// methods
	// ........................................................................
	
	public double getValue1()                    { return this.value1;   }
	public void   setValue1(final double value1) { this.value1 = value1; }

	public double getValue2()                    { return this.value2;   }
	public void   setValue2(final double value2) { this.value2 = value2; }

	public void add() {
		
		this.result = this.value1 + this.value2;
		this.operation = "+";
		
	}

	public void sub() {
		
		this.result = this.value1 - this.value2;
		this.operation = "-";
		
	}

	public void mlt() {
		
		this.result = this.value1 * this.value2;
		this.operation = "*";
		
	}

	public void div() {
		
		this.result = this.value1 / this.value2;
		this.operation = "/";
		
	}
	
	public double getResult() {
		
		return this.result;
		
	}

	public String getOperation() {
		
		return this.operation;
		
	}

}
