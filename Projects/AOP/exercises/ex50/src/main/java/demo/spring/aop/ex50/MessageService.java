/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.aop.ex50;

/**
 * The interface of the service
 * 
 * @author Franz Tost
 */
public interface MessageService {
	
	// methods
	// ........................................................................
	
	void setPrefix(String prefix);
	
	void setSuffix(String suffix);
	
	void service(String message);

}
