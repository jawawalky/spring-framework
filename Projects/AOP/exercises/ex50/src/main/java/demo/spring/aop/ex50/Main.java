/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2006 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.aop.ex50;

import demo.util.Demo;

/**
 * This is a simple demo, which shows you how you can configure your
 * beans.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors
	// ........................................................................

	private Main() { }

	// methods
	// ........................................................................

	private void runDemo() {

		Demo.log("Running demo ...");
		
		this.runAsPojo();
		this.runAsSpringBean();
		
		Demo.log("Finished.");

	}
	
	private void runAsPojo() {
		
		Demo.log("Running as POJO ...");
		
		// TODO
		//
		//  o Create a service object with 'new'.
		//
		//  o Set the prefix 'abc'.
		//
		//  o Set the suffix 'xyz'.
		//
		//  o Call the 'service()' method of the service.
		
	}
	
	private void runAsSpringBean() {
		
		Demo.log("Running as Spring Bean ...");
		
		// TODO
		//
		//  o Create a service object with the application context.
		//
		//  o Set the prefix 'abc'.
		//
		//  o Set the suffix 'xyz'.
		//
		//  o Call the 'service()' method of the service.
		
	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Main().runDemo();

	}

}
