/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2006 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.aop.ex15;

import demo.util.Demo;

/**
 * This is a simple demo, which shows you how you can configure your
 * beans.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors
	// ........................................................................

	private Main() { }

	// methods
	// ........................................................................

	private void runDemo() {

		Demo.log("Running demo ...");
		
		
		// TODO
		//
		//  o Create a 'ProxyFactory', which wraps a 'CalculatorImpl' object.
		//
		//  o Create a 'RegexpMethodPointcutAdvisor', which has a regular
		//    expression, that only accepts the 'mlt' method and which uses
		//    the 'Tracer' as its advice.
		//
		//  o Add the created 'RegexpMethodPointcutAdvisor' to the proxy
		//    factory.
		//
		//  o Take the proxy factory and create a proxy instance. Of what type
		//    is that proxy instance. What can you do with it?
		

		Demo.log("Finished.");

	}

	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Main().runDemo();

	}

}
