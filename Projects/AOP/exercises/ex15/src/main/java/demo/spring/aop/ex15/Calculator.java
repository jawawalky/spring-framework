/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.aop.ex15;

/**
 * The calculator interface.
 * 
 * @author Franz Tost
 *
 */
public interface Calculator {
	
	// methods
	// ........................................................................

	double getA();
	void   setA(double a);

	double getB();
	void   setB(double b);

	double add();
	double sub();
	double mlt();
	double div();

}
