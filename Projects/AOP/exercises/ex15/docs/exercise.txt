Programmatic Proxies
====================

A) The Application
==================

  'DemoApp':
  ----------

  1) Create a 'ProxyFactory', which wraps a 'CalculatorImpl' object.
  
  2) Create a 'RegexpMethodPointcutAdvisor', which has a regular
     expression, that only accepts the 'mlt' method and which uses
     the 'Tracer' as its advice.
     
  3) Add the created 'RegexpMethodPointcutAdvisor' to the proxy
     factory.
     
  4) Take the proxy factory and create a proxy instance. Of what type
     is that proxy instance. What can you do with it?