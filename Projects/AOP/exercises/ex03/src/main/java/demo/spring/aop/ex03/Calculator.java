/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.aop.ex03;

/**
 * @author Franz Tost
 *
 */
public interface Calculator {

	double getValue1();

	void setValue1(double value1);

	double getValue2();

	void setValue2(double value2);

	void add();

	void sub();

	void mlt();

	void div() throws Exception;
	
	double getResult();
	
	String getOperation();

}
