/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2006 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.aop.ex04;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import demo.util.Demo;

/**
 * This is a simple demo, which shows you how you can configure your
 * beans.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors
	// ........................................................................

	/**
	 * A private default constructor.
	 */
	private Main() {

		super();

	}

	// methods
	// ........................................................................

	/**
	 * The demo.
	 */
	private void runDemo() {

		Demo.log("Running demo ...");

		ApplicationContext context
			= new ClassPathXmlApplicationContext("beans.xml");

		Calculator calculator = (Calculator) context.getBean("calculator");
		
		
		// TODO
		//
		//  o Perform some calculations. Include calculations, which also
		//    produce exceptions.
		//
		//  o Retrieve the 'throwsCounter' and print its current values.
		
		
		Demo.log("Finished.");

	}

	private void printResult(Calculator calculator) {

		Demo.log(calculator.getValue1()
			+ " "
			+ calculator.getOperation()
			+ " "
			+ calculator.getValue2()
			+ " = "
			+ calculator.getResult());

	}

	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		Main app = new Main();
		app.runDemo();

	}

}
