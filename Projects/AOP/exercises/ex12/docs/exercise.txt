Generic Pointcuts
=================

A) Static Pointcuts
===================

  Static pointcuts only consider the method and its signature, i.e. the types
  of its parameter list. The do not take into account the values passed to
  the method at runtime. Static pointcuts show better performance add should
  be the preferred type of pointcut. 


  'StaticPointcut':
  -----------------

  1) Implement the interface 'Pointcut' on the class 'StaticPointcut'.
  
  2) The class filter of this pointcut should only let objects pass, which
     are derived from the interface 'Calculator'.
     
     Hint: Use an instance of an anonymous inner class derived from
           'ClassFilter'.
           
  3) The method matcher of this pointcut should match the 'add' and the
     'mlt' method.
     		
     Hint: Use an instance of an anonymous inner class derived from
           'MethodMatcher'.
           
  4) The method 'isRuntime()' of the method matcher must return 'false', since
     it is a static pointcut. That causes the method
     
       'public boolean matches(Method method, Class<?> type)'
       
     to be called, but not the method
     
       'public boolean matches(Method method, Class<?> type, Object[] args)'
       
  5) Implement the method 'matches(Method method, Class<?> type)' of the
     method matcher. Check the name of the passed method. Accept the method
     only, if its name is 'add' or 'mlt'.
     
  6) Let the method 'matches(Method method, Class<?> type, Object[] args)'
     return 'false'. It will not be called in a static pointcut.

     
B) Dynamic Pointcuts
====================

  Dynamic pointcuts allow the evaluation of the values passed to a method at
  runtime. This increases the possibilities for the selection of the matched
  methods, but it may slow down performance considerably!
  
  
  'DynamicPointcut':
  ------------------
  
  1) Implement the interface 'Pointcut' on the class 'DynamicPointcut'.
  
  2) The class filter of this pointcut should only let objects pass, which
     are derived from the interface 'Calculator'.
     
     Hint: Use an instance of an anonymous inner class derived from
           'ClassFilter'.
           
  3) The method matcher of this pointcut should only consider methods with
     one argument and where the argument is 0.0.
     		
     Hint: Use an instance of an anonymous inner class derived from
           'MethodMatcher'.
           
  4) The method 'isRuntime()' of the method matcher must return 'true', since
     it is a dynamic pointcut. That causes both methods
     
       'public boolean matches(Method method, Class<?> type)'
       
     and
     
       'public boolean matches(Method method, Class<?> type, Object[] args)'
       
     to be evaluated.
       
  5) Implement the method 'matches(Method method, Class<?> type)' of the
     method matcher. It can let all methods pass.
     
  6) Implement the method 'matches(Method method, Class<?> type, Object[] args)'
     so it checks the length of the argument list and it the first argument has
     the value 0.0.
