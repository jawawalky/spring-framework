/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.aop.ex12;


/**
 * A generic <code>Pointcut</code> implementation, which is not evaluated at
 * runtime, since the <code>isRuntime()</code> method of the
 * <code>MethodMatcher</code> returns <code>false</code>.
 * 
 * @author Franz Tost
 *
 */
public class StaticPointcut {
	
	// methods
	// ........................................................................

}
