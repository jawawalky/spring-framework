/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.aop.ex51;

import org.aspectj.lang.JoinPoint;

import demo.util.Demo;

/**
 * A simple method tracer.
 * 
 * @author Franz Tost
 * 
 */
public class TracingAspect {
	
	// advices
	// ........................................................................

	public void enter(final JoinPoint joinPoint) {
		
		Demo.log(joinPoint.toString());
		
	}

}
