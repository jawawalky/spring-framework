/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex12;

import java.util.Map;
import java.util.Properties;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import demo.util.Demo;

/**
 * This demo is about dependency injection and a property of type
 * <code>java.util.Properties</code>.
 * 
 * @author Franz Tost
 *
 */
public class DemoApp {

	// constructors
	// ........................................................................

	/**
	 * A private default constructor.
	 */
	private DemoApp() {
		
		super();
		
	}

	// methods
	// ........................................................................

	/**
	 * The demo.
	 */
	private void runDemo() {

		Demo.log("Running demo ...");
		
		ApplicationContext context =
		    new ClassPathXmlApplicationContext("beans.xml");
		
		SpaceShip heartOfGold = (SpaceShip) context.getBean("heartOfGold");

		
		Demo.log("Settings");
		
		Properties settings = heartOfGold.getSettings();
		for (Object key : settings.keySet()) {
			
			Demo.log("  " + key + " = " + settings.getProperty((String) key));
			
		} // for

		
		Demo.log("Crew");
		
		for (String member : heartOfGold.getCrew()) {
			
			Demo.log("  " + member);
			
		} // for

		
		Demo.log("Decks");
		
		for (String deck : heartOfGold.getDecks()) {
			
			Demo.log("  " + deck);
			
		} // for

		
		Demo.log("Tasks");
		
		for (Map.Entry<String, String> task : heartOfGold.getTasks().entrySet()) {
			
			Demo.log("  " + task.getKey() + " = " + task.getValue());
			
		} // for
		
		Demo.log("Finished.");

	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		DemoApp app = new DemoApp();
		app.runDemo();

	}

}
