/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex52;

import java.util.Map;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.config.Scope;

/**
 * A per-thread-scope.
 * 
 * @author Franz Tost
 *
 */
public class ThreadScope implements Scope {
	
	// fields
	// ........................................................................
	
	private ThreadLocal<Map<String, Object>> threadContext
		= new ThreadLocal<Map<String,Object>>();

	// methods
	// ........................................................................

	@Override
	public Object get(String name, ObjectFactory<?> objectFactory) {
		
		// TODO
		//
		//  o Return the value referenced by the specified name.
		//
		//  o If the value does not exist, then create it with the specified
		//    object factory and bind it in the value map.
		
		return null;
		
	}

	@Override
	public String getConversationId() {
		
		// TODO
		//
		//  o Return the ID of the current thread.
		
		return null;
		
	}

	@Override
	public void registerDestructionCallback(String name, Runnable callback) {
		
		// Not supported

	}

	@Override
	public Object remove(String name) {
		
		// TODO
		//
		//  o Remove the value referenced by the specified name from
		//    the value map.
		//
		//  o Return the removed value.
		
		return null;
		
	}

	@Override
	public Object resolveContextualObject(String name) {
		
		// TODO
		//
		//  o Retrieve the values map and return the value referenced
		//    by the specified name.
		//
		//  o Return that value.
		
		return null;
		
	}
	
	private Map<String, Object> getValues() {
		
		// TODO
		//
		//  o Get the map from the thread context.
		//
		//  o If the map is 'null', then create a new 'HashMap' and set it
		//    on the thread context.
		//
		//  o Return the map.
		
		return null;
		
	}

}
