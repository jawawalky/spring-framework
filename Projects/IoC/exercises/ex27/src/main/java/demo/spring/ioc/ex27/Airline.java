/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex27;

/**
 * A JavaBean modeling an airline.
 * 
 * @author Franz Tost
 *
 */
public class Airline {

	// fields
	// ........................................................................

	private String code;

	private String name;
	
	// TODO
	//
	//  o Add the following properties to the bean
	//
	//      o homeAirport    : Airport
	//      o serviceAirport : Airport
	//
	//  o Both properties should be autowired.
	//
	//  o Since two different airport beans are defined, we need to qualify
	//    which bean should be autowired. Add a @Qualifier annotation to the
	//    autowired references.

	// constructors
	// ........................................................................

	public Airline() { }

	public Airline(
		final String  code,
		final String  name,
		final Airport homeAirport
	) {

		super();

		this.code = code;
		this.name = name;

	}

	// methods
	// ........................................................................

	public String getCode()                  { return this.code; }
	public void   setCode(final String code) { this.code = code; }

	public String getName()                  { return this.name; }
	public void   setName(final String name) { this.name = name; }

	@Override public String toString() {
		
		// TODO
		//
		//  o Complete the 'toString()' method.

		return this.getName() +
			" (" + this.getCode() + ")";

	}

}
