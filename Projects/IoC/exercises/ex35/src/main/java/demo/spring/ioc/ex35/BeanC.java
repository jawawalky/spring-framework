/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex35;

/**
 * An example bean, which has a default <i>initialization</i> and
 * default <i>destruction</i> method.
 * <p/>
 * <b>Note:</b>
 * <blockquote>
 * 	No Spring import are needed in this class.
 * </blockquote>
 * 
 * @author Franz Tost
 *
 */
public class BeanC extends Bean0 {
	
	// methods
	// ........................................................................
	
	// TODO
	//
	//  o Add a method 'public void defaultInit()', which prints some log message,
	//    when it was called.
	//
	//  o Add a method 'public void defaultExit()', which prints some log message,
	//    when it was called.
	//
	//  Note:
	//
	//    The methods will be configured as default initializer and default
	//    destructor methods, in the beans container.

}
