/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex35;

/**
 * An example bean, which uses life cycle annotations.
 * 
 * @author Franz Tost
 *
 */
public class BeanB extends Bean0 {
	
	// methods
	// ........................................................................
	
	// TODO
	//
	//  o Add a method 'public void init()', which prints some log message,
	//    when it was called.
	//
	//  o Add a method 'public void exit()', which prints some log message,
	//    when it was called.
	//
	//  Note:
	//
	//    The methods will be configured as initializer and destructor methods,
	//    in the beans container.

}
