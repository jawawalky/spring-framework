/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex35;

import demo.util.Demo;


/**
 * A base class for the other bean classes.
 * 
 * @author Franz Tost
 *
 */
public class Bean0 {
	
	// fields
	// ........................................................................
	
	private int value;

	// methods
	// ........................................................................

	public int getValue() {
		
		return this.value;
		
	}

	public void setValue(int value) {
		
		this.value = value;
		
	}
	
	protected void print(String message) {
		
		Demo.log(this.getClass().getSimpleName() + ": " + message);
		
	}

}
