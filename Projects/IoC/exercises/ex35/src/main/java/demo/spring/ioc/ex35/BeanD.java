/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex35;


/**
 * An example bean, which is life cycle aware by configuration.
 * <p/>
 * <b>Note:</b>
 * <blockquote>
 * 	No Spring import are needed in this class.
 * </blockquote>
 * 
 * @author Franz Tost
 *
 */
public class BeanD extends Bean0 {
	
	// methods
	// ........................................................................

	// TODO
	//
	//  o Add a method 'public void init()', which prints some log message,
	//    when it was called. Annotate it with '@PostConstruct'.
	//
	//  o Add a method 'public void exit()', which prints some log message,
	//    when it was called. Annotate it with '@PreDestroy'.
	
}
