/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex35;


/**
 * An example bean using the life cycle interfaces
 * <code>InitializingBean</code> and <code>DisposableBean</code>.
 * 
 * @author Franz Tost
 * 
 * TODO
 * 
 *  o Implement the two interfaces 'InitializingBean' and 'DisposableBean'.
 *  
 *  o Print log messages, which tell you when the callback methods were called.
 *
 */
public class BeanA extends Bean0 {
}
