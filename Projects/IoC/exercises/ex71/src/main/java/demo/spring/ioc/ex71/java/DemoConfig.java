/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex71.java;

import org.springframework.context.annotation.Configuration;

/**
 * A Java-based configuration.
 * 
 * @author Franz Tost
 *
 */
@Configuration
public class DemoConfig {

	// beans
	// ........................................................................
	
	// TODO
	//
	//  o Configure a Spanish dictionary bean.
	//
	//  o Configure a translator bean.
	//

}
