/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex71.java;

/**
 * A simple translator implementation.
 * 
 * @author Franz Tost
 */
public class Translator {
	
	// fields
	// ........................................................................
	
	// TODO
	//
	//  o Inject/Auto-wire a dictionary here.
	//
	
	// methods
	// ........................................................................
	
	public String translate(final String wordInEnglish) {
		
		// TODO
		//
		//  o Use the injected dictionary to perform the translation.
		//
		//  o Return the translated word.
		//
		return null;
		
	}

}
