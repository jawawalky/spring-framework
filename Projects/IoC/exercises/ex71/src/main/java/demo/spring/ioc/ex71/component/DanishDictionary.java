/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex71.component;

import demo.spring.ioc.ex71.SimpleDictionary;

/**
 * A translator from English to Danish.
 * 
 * @author Franz Tost
 * 
 * TODO
 * 
 *  o Make this class a component.
 */
public class DanishDictionary extends SimpleDictionary {
	
	// constructors
	// ........................................................................
	
	public DanishDictionary() {
		
		super();
		
		this.addTranslation("rain", "regn");
		this.addTranslation("sun",  "sol");
		this.addTranslation("wind", "vind");
		this.addTranslation("snow", "sne");
		
	}

}
