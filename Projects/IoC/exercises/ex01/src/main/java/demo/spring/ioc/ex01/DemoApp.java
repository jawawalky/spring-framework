/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex01;

import demo.util.Demo;

/**
 * This is a simple demo, which shows you how you can configure your
 * beans.
 * 
 * @author Franz Tost
 *
 */
public class DemoApp {

	// constructors
	// ........................................................................

	/**
	 * A private default constructor.
	 */
	private DemoApp() {
		
		super();
		
	}

	// methods
	// ........................................................................

	/**
	 * The demo.
	 */
	private void runDemo() {

		Demo.log("Running demo ...");
		
		// TODO
		//
		//  o Create an 'ApplicationContext' for the two configuration files
		//    'germany.xml' and 'spain.xml'. The files can be found on the
		//    class path.
		//
		//    Hint: Use the class 'ClassPathXmlApplicationContext'.
		//
		//  o Take the context and create the beans for the IDs
		//
		//      o 'bcn'
		//      o 'fra'
		//      o 'mad'
		//
		//  o Print the airport object on the console.
		//
		
		Demo.log("Finished.");

	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		DemoApp app = new DemoApp();
		app.runDemo();

	}

}
