/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex01;

/**
 * A JavaBean modeling an airport.
 * 
 * @author Franz Tost
 *
 */
public class Airport {

	// fields
	// ........................................................................
	
	// TODO
	//
	//  o Add the following fields to the airport class
	//
	//      o 'code'
	//      o 'name'
	//      o 'city'
	//
	//    All fields should be String values.
	//

	// constructors
	// ........................................................................
	
	// TODO
	//
	//  o Create at least a default constructor.
	//
	//  o Create further constructors for your convenience, if you want.
	//

	// methods
	// ........................................................................
	
	// TODO
	//
	//  o Create getter and setter methods for the fields of this class.
	//
	//  o Override the 'toString()' method, which should display the object
	//    values.
	//

}
