/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex16;

import demo.util.Demo;

/**
 * An abstract class to be subclassed by code generation for lookup method
 * injection.
 * 
 * @author Franz Tost
 *
 */
public abstract class Calculator {
	
	// methods
	// ........................................................................
	
	// TODO
	//
	//  o Create an abstract method called 'createOpertation()'. It should
	//    return a 'StatefulOperation' object.
	
	
	public void calculate(double a, double b) {
		
		// TODO
		//
		//  o Create a 'StatefulOperation' object with the abstract method
		//    'createOperation()'.
		//
		//  o Set the values of this object.
		//
		//  o Call the 'evaluate()' method of the operation and print the
		//    result on the console.
		
	}
	
}
