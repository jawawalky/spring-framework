/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex16;

/**
 * A stateful operation, so we need a separate instance for each operation
 * to be performed.
 * 
 * @author Franz Tost
 *
 */
public class StatefulAdd implements StatefulOperation {
	
	// fields
	// ........................................................................
	
	private double a;
	
	private double b;

	// methods
	// ........................................................................
	
	public double getA() {
		
		return this.a;
		
	}

	@Override
	public void setA(double a) {
		
		this.a = a;
		
	}

	public double getB() {
		
		return this.b;
		
	}

	@Override
	public void setB(double b) {
		
		this.b = b;
		
	}
	
	@Override
	public double evaluate() {
		
		return this.getA() + this.getB();
		
	}

	@Override
	public String toString() {
		
		return "+";
		
	}

}
