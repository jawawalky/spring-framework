/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex70.component;

import org.junit.Test;

/**
 * A unit test for the Java-based configuration and component scan.
 * 
 * @author Franz Tost
 *
 */
public class DemoTest {
	
	// tests
	// ........................................................................

	@Test public void test() {
		
		// TODO
		//
		//  o Create an application context, which uses the Java-based
		//    configuration class '...component.DemoConfig'.
		//
		//    Hint: Use the class 'AnnotationConfigApplicationContext'.
		//
		//  o Retrieve a 'Translator' from the context.
		//
		//  o Test some translations.
		//
		
	}

}
