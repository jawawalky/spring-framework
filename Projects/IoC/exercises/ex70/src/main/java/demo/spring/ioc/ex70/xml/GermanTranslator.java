/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex70.xml;

import demo.spring.ioc.ex70.SimpleTranslator;

/**
 * A translator from English to German.
 * 
 * @author Franz Tost
 *
 */
public class GermanTranslator extends SimpleTranslator {
	
	// constructors
	// ........................................................................
	
	public GermanTranslator() {
		
		super();
		
		this.addTranslation("rain", "Regen");
		this.addTranslation("sun",  "Sonne");
		this.addTranslation("wind", "Wind");
		this.addTranslation("snow", "Schnee");
		
	}

}
