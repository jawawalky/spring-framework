/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex70.component;

import org.springframework.stereotype.Component;

import demo.spring.ioc.ex70.SimpleTranslator;

/**
 * A translator from English to Danish.
 * 
 * @author Franz Tost
 *
 */
@Component
public class DanishTranslator extends SimpleTranslator {
	
	// constructors
	// ........................................................................
	
	public DanishTranslator() {
		
		super();
		
		this.addTranslation("rain", "regn");
		this.addTranslation("sun",  "sol");
		this.addTranslation("wind", "vind");
		this.addTranslation("snow", "sne");
		
	}

}
