/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex70.java;

/**
 * A Java-based configuration.
 * 
 * @author Franz Tost
 *
 * TODO
 * 
 *  o Make this class a configuration class.
 */
public class DemoConfig {

	// beans
	// ........................................................................
	
	// TODO
	//
	//  o Declare a 'Translator' bean, which performs Spanish translations.
	//

}
