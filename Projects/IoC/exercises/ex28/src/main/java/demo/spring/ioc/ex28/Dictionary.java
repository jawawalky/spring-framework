/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex28;

/**
 * An interface for dictionary implementations.
 * 
 * @author Franz Tost
 *
 */
public interface Dictionary {
	
	// methods
	// ........................................................................
	
	/**
	 * Translates a word from one language into another language.
	 * 
	 * @param word the word in the source language.
	 * 
	 * @return The word in the target language.
	 */
	String translate(String word);

}
