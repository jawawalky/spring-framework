/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex10;


/**
 * A JavaBean modeling an airline.
 * 
 * @author Franz Tost
 *
 * TODO
 * 
 *  o Add a bean property 'homeAirport' of type 'Airport'.
 */
public class Airline {

	// fields
	// ........................................................................

	private String code;
	private String name;

	// constructors
	// ........................................................................

	public Airline() { }

	// TODO
	//
	//  o Set the home airport of the airline.
	
	public Airline(
		final String  code,
		final String  name,
		final Airport homeAirport
	) {

		super();

		this.code = code;
		this.name = name;

	}

	// methods
	// ........................................................................

	public String  getCode()                  { return this.code; }
	public void    setCode(final String code) { this.code = code; }

	public String  getName()                  { return this.name; }
	public void    setName(final String name) { this.name = name; }

	@Override
	public String toString() {
		
		// TODO
		//
		//  o Returns a string composed of airline name, code and the name
		//    of the home airport.

		return null;

	}

}
