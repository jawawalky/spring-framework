/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex10;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import demo.util.Demo;

/**
 * This demo teaches you different ways of dependency injection.
 * 
 * @author Franz Tost
 *
 */
public class DemoApp {

	// constructors
	// ........................................................................

	private DemoApp() { }

	// methods
	// ........................................................................

	private void runDemo() {

		Demo.log("Running demo ...");
		
		this.properties();
		this.constructorArgs();
		
		Demo.log("Finished.");

	}
	
	private void properties() {
		
		Demo.log("A) Dependency injection by properties ...");
		
		ApplicationContext context =
		    new ClassPathXmlApplicationContext("beans01.xml");
		
		Demo.log(((Airport) context.getBean("fra")).toString());
		
	}
	
	private void constructorArgs() {
		
		Demo.log("B) Dependency injection by constructor arguments ...");
		
		ApplicationContext context =
		    new ClassPathXmlApplicationContext("beans02.xml");
		
		Demo.log(((Airport) context.getBean("fra")).toString());
		Demo.log(((Airline) context.getBean("lh")).toString());
		
	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new DemoApp().runDemo();

	}

}
