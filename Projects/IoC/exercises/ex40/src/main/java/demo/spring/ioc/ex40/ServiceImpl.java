/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex40;

import demo.util.Demo;

/**
 * The service implementation.
 * 
 * @author Franz Tost
 *
 */
public class ServiceImpl implements Service {
	
	// methods
	// ........................................................................

	@Override public void doService() { Demo.log("Executing service ..."); }

}
