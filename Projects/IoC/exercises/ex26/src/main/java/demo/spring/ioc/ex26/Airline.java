/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex26;

/**
 * A JavaBean modeling an airline.
 * 
 * @author Franz Tost
 *
 */
public class Airline {

	// fields
	// ........................................................................

	private String code;

	private String name;
	
	// TODO
	//
	//  o Add a field 'homeAirport' of type 'Airport'.
	//
	//  o Annotate the field with '@Autowired'.

	// constructors
	// ........................................................................

	public Airline() { }

	public Airline(
		final String  code,
		final String  name,
		final Airport homeAirport
	) {

		super();

		this.code = code;
		this.name = name;
		
		// TODO
		//
		//  o Set the home airport.

	}

	// methods
	// ........................................................................

	public String getCode()                  { return this.code; }
	public void   setCode(final String code) { this.code = code; }

	public String getName()                  { return this.name; }
	public void   setName(final String name) { this.name = name; }
	
	// TODO
	//
	//  o Add getter and setter methods for the home airport.

	@Override public String toString() {

		// TODO
		//
		//  o Return the name and code of the airline and the city of
		//    its home airport.
		
		return null;

	}

}
