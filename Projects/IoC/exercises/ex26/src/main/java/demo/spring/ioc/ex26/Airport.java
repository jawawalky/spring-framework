/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex26;

/**
 * A JavaBean modeling an airport.
 * 
 * @author Franz Tost
 *
 */
public class Airport {

	// fields
	// ........................................................................

	private String code;
	private String name;
	private String city;

	// constructors
	// ........................................................................

	public Airport() { }

	public Airport(
		final String code,
		final String name,
		final String city
	) {

		super();

		this.code = code;
		this.name = name;
		this.city = city;

	}

	// methods
	// ........................................................................

	public String getCode()                  { return this.code; }
	public void   setCode(final String code) { this.code = code; }

	public String getName()                  { return this.name; }
	public void   setName(final String name) { this.name = name; }

	public String getCity()                  { return this.city; }
	public void   setCity(final String city) { this.city = city; }

	@Override public String toString() {

		return this.getName() + " (" + this.getCode() + ")";

	}

}
