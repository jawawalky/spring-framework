/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex20;


/**
 * A JavaBean modeling an airline.
 * 
 * @author Franz Tost
 *
 */
public class Airline {

	// fields
	// ........................................................................

	private String code;

	private String name;

	private Airport homeAirport;
	
	// constructors
	// ........................................................................

	public Airline() {

		super();

	}

	public Airline(String code, String name, Airport homeAirport) {

		super();

		this.code = code;
		this.name = name;
		this.homeAirport = homeAirport;

	}

	// methods
	// ........................................................................

	public String getCode() {

		return code;

	}

	public void setCode(String code) {

		this.code = code;

	}

	public String getName() {

		return name;

	}

	public void setName(String name) {

		this.name = name;

	}

	public Airport getHomeAirport() {
		
		return homeAirport;
		
	}

	public void setHomeAirport(Airport homeAirport) {
		
		this.homeAirport = homeAirport;
		
	}
	
	@Override
	public String toString() {

		return this.getName() +
			" (" + this.getCode() + ", " +
			this.getHomeAirport().getCity() + ")";

	}

}
