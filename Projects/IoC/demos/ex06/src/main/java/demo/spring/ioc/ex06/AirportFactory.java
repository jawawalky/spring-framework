/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex06;

/**
 * The factory class.
 * 
 * @author Franz Tost
 *
 */
public class AirportFactory {
	
	// methods
	// ........................................................................
	
	public static Airport createAirport(
		final String code,
		final String name,
		final String city
	) {
		
		return new Airport(code, name, city);
		
	}

}
