/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex50;

/**
 * The bean class.
 * 
 * @author Franz Tost
 *
 */
public class Bean {
	
	// fields
	// ........................................................................
	
	private static int instanceCount;
	
	private int number;
	
	// constructors
	// ........................................................................
	
	public Bean() {
		
		super();
		
		instanceCount++;
		this.number = instanceCount;
		
	}

	// methods
	// ........................................................................
	
	@Override
	public String toString() {
		
		return "Bean " + this.number;
		
	}

}
