/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex27;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;


/**
 * A JavaBean modeling an airline.
 * 
 * @author Franz Tost
 *
 */
public class Airline {

	// fields
	// ........................................................................

	private String code;

	private String name;

	@Autowired
	@Qualifier("home")
	private Airport homeAirport;
	
	@Autowired
	@Qualifier("service")
	private Airport serviceAirport;
	
	// constructors
	// ........................................................................

	public Airline() { }

	public Airline(
		final String  code,
		final String  name,
		final Airport homeAirport
	) {

		super();

		this.code        = code;
		this.name        = name;
		this.homeAirport = homeAirport;

	}

	// methods
	// ........................................................................

	public String  getCode()                  { return this.code; }
	public void    setCode(final String code) { this.code = code; }

	public String  getName()                  { return this.name; }
	public void    setName(final String name) { this.name = name; }

	public Airport getHomeAirport()                      { return this.homeAirport;    }
	public void    setHomeAirport(final Airport airport) { this.homeAirport = airport; }
	
	public Airport getServiceAirport()                      { return this.serviceAirport;    }
	public void    setServiceAirport(final Airport airport) { this.serviceAirport = airport; }
	
	@Override public String toString() {

		return this.getName() +
			" (" + this.getCode() + ", " +
			"home = " + this.getHomeAirport().getCity() + ", " +
			"service = " + this.getServiceAirport().getCity() + ")";

	}

}
