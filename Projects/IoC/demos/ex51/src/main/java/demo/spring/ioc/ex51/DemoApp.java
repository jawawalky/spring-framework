/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex51;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import demo.util.Demo;

/**
 * This demo creates a bean with singleton scope, which is the default scope.
 * 
 * @author Franz Tost
 *
 */
public class DemoApp {

	// constructors
	// ........................................................................

	/**
	 * A private default constructor.
	 */
	private DemoApp() {
		
		super();
		
	}

	// methods
	// ........................................................................

	/**
	 * The demo.
	 */
	private void runDemo() {

		Demo.log("Running demo ...");
		
		ApplicationContext context =
		    new ClassPathXmlApplicationContext("beans.xml");
		
		Demo.log(((Bean) context.getBean("bean1")).toString());
		Demo.log(((Bean) context.getBean("bean2")).toString());
		Demo.log(((Bean) context.getBean("bean2")).toString());
		Demo.log(((Bean) context.getBean("bean1")).toString());
		
		Demo.log("Finished.");

	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		DemoApp app = new DemoApp();
		app.runDemo();

	}

}
