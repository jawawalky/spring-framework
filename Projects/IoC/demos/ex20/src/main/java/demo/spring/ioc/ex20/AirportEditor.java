/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex20;

import java.beans.PropertyEditorSupport;
import java.util.StringTokenizer;

/**
 * An editor class for the {@linkplain Airport} class.
 * 
 * @author Franz Tost
 *
 */
public class AirportEditor extends PropertyEditorSupport {
	
	// methods
	// ........................................................................

	/**
	 * Converts a string of the form
	 * <blockquote>
	 * 	<code><i>code</i>, <i>name</i>, <i>city</i></code>
	 * </blockquote>
	 * into an {@linkplain Airport} object and sets it as the property value.
	 * 
	 * @param text the string information of the airport.
	 */
	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		
		if (text == null) {
			
			return;
			
		} // if
		
		text = text.trim();
		
		StringTokenizer tokenizer = new StringTokenizer(text, ",");
		
		String code = tokenizer.nextToken();
		String name = tokenizer.nextToken();
		String city = tokenizer.nextToken();
		
		this.setValue(new Airport(code, name, city));
		
	}

}
