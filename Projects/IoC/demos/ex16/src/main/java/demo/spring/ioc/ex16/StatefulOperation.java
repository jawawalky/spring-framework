/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex16;

/**
 * The interface for the stateful operations.
 * 
 * @author Franz Tost
 *
 */
public interface StatefulOperation {
	
	// methods
	// ........................................................................

	void setA(double a);

	void setB(double b);

	double evaluate();

}