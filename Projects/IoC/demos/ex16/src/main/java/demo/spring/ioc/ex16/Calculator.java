/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex16;

import demo.util.Demo;

/**
 * An abstract class to be subclassed by code generation for lookup method
 * injection.
 * 
 * @author Franz Tost
 *
 */
public abstract class Calculator {
	
	// methods
	// ........................................................................
	
	public void calculate(double a, double b) {
		
		StatefulOperation operation = this.createOperation();
		
		operation.setA(a);
		operation.setB(b);
		
		Demo.log(a + " " + operation + " " + b + " = " + operation.evaluate());
			
	}
	
	protected abstract StatefulOperation createOperation();

}
