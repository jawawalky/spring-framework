/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex70.java;

import demo.spring.ioc.ex70.SimpleTranslator;

/**
 * A translator from English to Spanish.
 * 
 * @author Franz Tost
 *
 */
public class SpanishTranslator extends SimpleTranslator {
	
	// constructors
	// ........................................................................
	
	public SpanishTranslator() {
		
		super();
		
		this.addTranslation("rain", "lluvia");
		this.addTranslation("sun",  "sol");
		this.addTranslation("wind", "viento");
		this.addTranslation("snow", "nieve");
		
	}

}
