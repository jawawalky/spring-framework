/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex70.java;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import demo.spring.ioc.ex70.Translator;

/**
 * A Java-based configuration.
 * 
 * @author Franz Tost
 *
 */
@Configuration
public class DemoConfig {

	// beans
	// ........................................................................

	@Bean public Translator translator() { return new SpanishTranslator(); }

}
