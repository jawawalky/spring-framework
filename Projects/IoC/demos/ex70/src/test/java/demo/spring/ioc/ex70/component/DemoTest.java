/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex70.component;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import demo.spring.ioc.ex70.Translator;

/**
 * A unit test for the Java-based configuration and component scan.
 * 
 * @author Franz Tost
 *
 */
public class DemoTest {
	
	// tests
	// ........................................................................

	@Test public void test() {
		
		try (AnnotationConfigApplicationContext context =
		    new AnnotationConfigApplicationContext(DemoConfig.class)
		) {
		
			Translator translator = context.getBean(Translator.class);
			
			assertNotNull(translator);
			assertEquals("regn", translator.translate("rain"));
			assertEquals("sol",  translator.translate("sun"));
			assertEquals("vind", translator.translate("wind"));
			assertEquals("sne",  translator.translate("snow"));
			
		} // try-with-resources

	}

}
