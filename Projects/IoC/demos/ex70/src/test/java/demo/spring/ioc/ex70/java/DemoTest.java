/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex70.java;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import demo.spring.ioc.ex70.Translator;

/**
 * A unit test for the Java-based configuration.
 * 
 * @author Franz Tost
 *
 */
public class DemoTest {
	
	// tests
	// ........................................................................

	@Test public void test() {
		
		try (AnnotationConfigApplicationContext context =
		    new AnnotationConfigApplicationContext(DemoConfig.class)
		) {
		
			Translator translator = context.getBean(Translator.class);
			
			assertNotNull(translator);
			assertEquals("lluvia", translator.translate("rain"));
			assertEquals("sol",    translator.translate("sun"));
			assertEquals("viento", translator.translate("wind"));
			assertEquals("nieve",  translator.translate("snow"));
			
		} // try-with-resources

		///////////////////////////////////////////////////////////////////////
		//
		// Note:
		//
		// Alternative context definitions would be
		//
		//   AnnotationConfigApplicationContext context =
		//       new AnnotationConfigApplicationContext(DemoConfig.class);
		//
		// or
		//
		//   AnnotationConfigApplicationContext context =
		//       new AnnotationConfigApplicationContext("demo.spring.ioc.ex70.java");
		//
		// or
		//
		//   AnnotationConfigApplicationContext context =
		//       new AnnotationConfigApplicationContext();
		//   context.register(DemoConfig.class);
		//   context.refresh();
		//
		// or
		//
		//
		//   AnnotationConfigApplicationContext context =
		//       new AnnotationConfigApplicationContext();
		//   context.scan("demo.spring.ioc.ex70.java");
		//   context.refresh();
		//
		///////////////////////////////////////////////////////////////////////
		
	}

}
