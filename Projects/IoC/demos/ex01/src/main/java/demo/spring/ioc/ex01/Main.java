/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex01;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import demo.util.Demo;

/**
 * This is a simple demo, which shows you how you can configure your
 * beans.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors
	// ........................................................................

	/**
	 * A private default constructor.
	 */
	private Main() {
		
		super();
		
	}

	// methods
	// ........................................................................

	/**
	 * The demo.
	 */
	private void runDemo() {

		Demo.log("Running demo ...");
		
		try (
			ClassPathXmlApplicationContext context =
			    new ClassPathXmlApplicationContext("germany.xml", "spain.xml");
		) {
		
			Airport bcn = (Airport) context.getBean("bcn");
			Airport fra = (Airport) context.getBean("fra");
			Airport mad = (Airport) context.getBean("mad");
			
			Demo.log(mad.toString());
			Demo.log(fra.toString());
			Demo.log(bcn.toString());
			
		} // try
		
		Demo.log("Finished.");

	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		Main app = new Main();
		app.runDemo();

	}

}
