/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex08;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import demo.util.Demo;

/**
 * This demo shows how to import configuration files into other configuration
 * files.
 * 
 * @author Franz Tost
 *
 */
public class DemoApp {

	// constructors
	// ........................................................................

	/**
	 * A private default constructor.
	 */
	private DemoApp() {
		
		super();
		
	}

	// methods
	// ........................................................................

	/**
	 * The demo.
	 */
	private void runDemo() {

		Demo.log("Running demo ...");
		
		ApplicationContext context =
		    new ClassPathXmlApplicationContext("airports.xml");
		
		Airport bcn = (Airport) context.getBean("bcn");
		Airport fra = (Airport) context.getBean("fra");
		Airport mad = (Airport) context.getBean("mad");
		Airport lhr = (Airport) context.getBean("lhr");
		
		Demo.log(mad.toString());
		Demo.log(fra.toString());
		Demo.log(bcn.toString());
		Demo.log(lhr.toString());
		
		Demo.log("Finished.");

	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		DemoApp app = new DemoApp();
		app.runDemo();

	}

}
