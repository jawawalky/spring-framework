/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex18;

/**
 * A class modeling a skyscraper.
 * 
 * @author Franz Tost
 *
 */
public class Skyscraper extends Building {
	
	// fields
	// ........................................................................
	
	private double height;

	// methods
	// ........................................................................
	
	public double getHeight()                    { return this.height;   }
	public void   setHeight(final double height) { this.height = height; }
	
	@Override public String toString() {
		
		return super.toString() + ", " + this.getHeight();
		
	}
	
}
