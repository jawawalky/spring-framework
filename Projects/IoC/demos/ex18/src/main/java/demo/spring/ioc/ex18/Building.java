/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex18;

/**
 * A base class for buildings.
 * 
 * @author Franz Tost
 *
 */
public class Building {
	
	// fields
	// ........................................................................
	
	private String name;
	
	private String type;

	// methods
	// ........................................................................
	
	public String getName()                  { return this.name; }
	public void   setName(final String name) { this.name = name; }

	public String getType()                  { return this.type; }
	public void   setType(final String type) { this.type = type; }

	@Override public String toString() {
		
		return this.getName() + ", " + this.getType();
		
	}
	
}
