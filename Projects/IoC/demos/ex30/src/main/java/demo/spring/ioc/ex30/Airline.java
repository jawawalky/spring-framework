/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex30;

import org.springframework.beans.factory.annotation.Autowired;


/**
 * A JavaBean modeling an airline.
 * 
 * @author Franz Tost
 *
 */
public class Airline {

	// fields
	// ........................................................................

	private String code;

	private String name;

	@Autowired
	@Home
	private Airport homeAirport;
	
	@Autowired
	@Service
	private Airport serviceAirport;
	
	// constructors
	// ........................................................................

	public Airline() {

		super();

	}

	public Airline(String code, String name, Airport homeAirport) {

		super();

		this.code = code;
		this.name = name;
		this.homeAirport = homeAirport;

	}

	// methods
	// ........................................................................

	public String getCode() {

		return code;

	}

	public void setCode(String code) {

		this.code = code;

	}

	public String getName() {

		return name;

	}

	public void setName(String name) {

		this.name = name;

	}

	public Airport getHomeAirport() {
		
		return homeAirport;
		
	}

	public void setHomeAirport(Airport homeAirport) {
		
		this.homeAirport = homeAirport;
		
	}
	
	public Airport getServiceAirport() {
		
		return serviceAirport;
		
	}

	public void setServiceAirport(Airport serviceAirport) {
		
		this.serviceAirport = serviceAirport;
		
	}
	
	@Override
	public String toString() {

		return this.getName() +
			" (" + this.getCode() + ", " +
			"home = " + this.getHomeAirport().getCity() + ", " +
			"service = " + this.getServiceAirport().getCity() + ")";

	}

}
