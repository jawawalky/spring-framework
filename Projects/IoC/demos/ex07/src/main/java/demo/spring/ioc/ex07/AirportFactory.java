/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex07;

/**
 * The factory class.
 * 
 * @author Franz Tost
 *
 */
public class AirportFactory {
	
	// methods
	// ........................................................................
	
	public Airport createAirport(String code, String name, String city) {
		
		return new Airport(code, name, city);
		
	}

}
