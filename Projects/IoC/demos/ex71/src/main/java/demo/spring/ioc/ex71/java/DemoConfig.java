/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex71.java;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import demo.spring.ioc.ex71.Dictionary;

/**
 * A Java-based configuration.
 * 
 * @author Franz Tost
 *
 */
@Configuration
public class DemoConfig {

	// beans
	// ........................................................................

	@Bean public Dictionary dictionary() { return new SpanishDictionary(); }

	@Bean public Translator translator() { return new Translator();        }

}
