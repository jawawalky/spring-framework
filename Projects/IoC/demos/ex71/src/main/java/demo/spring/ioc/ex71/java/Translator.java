/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex71.java;

import org.springframework.beans.factory.annotation.Autowired;

import demo.spring.ioc.ex71.Dictionary;

/**
 * A simple translator implementation.
 * 
 * @author Franz Tost
 */
public class Translator {
	
	// fields
	// ........................................................................
	
	@Autowired private Dictionary dictionary;
	
	// Note:
	//
	//  You can use the annotation
	//
	//    'javax.inject.Inject'
	//
	//  instead of
	//
	//    'org.springframework.beans.factory.annotation.Autowired'
	//

	// methods
	// ........................................................................
	
	public String translate(final String wordInEnglish) {
		
		return this.dictionary.translate(wordInEnglish);
		
	}

}
