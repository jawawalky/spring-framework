/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex71;

import java.util.HashMap;
import java.util.Map;

/**
 * A basic, simple translator implementation.
 * 
 * @author Franz Tost
 */
public class SimpleDictionary implements Dictionary {
	
	// fields
	// ........................................................................
	
	private Map<String, String> dictionary = new HashMap<>();

	// methods
	// ........................................................................
	
	protected void addTranslation(
		final String wordInEnglish,
		final String translatedWord
	) {
		
		this.dictionary.put(wordInEnglish, translatedWord);
		
	}
	
	@Override public String translate(final String wordInEnglish) {
		
		return this.dictionary.get(wordInEnglish);
		
	}

}
