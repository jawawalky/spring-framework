/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex71.xml;

import demo.spring.ioc.ex71.Dictionary;

/**
 * A simple translator implementation.
 * 
 * @author Franz Tost
 */
public class Translator {
	
	// fields
	// ........................................................................
	
	private Dictionary dictionary;

	// methods
	// ........................................................................
	
	public Dictionary getDictionary()                      { return this.dictionary;       }
	public void       setDictionary(Dictionary dictionary) { this.dictionary = dictionary; }
	
	public String translate(final String wordInEnglish) {
		
		return this.dictionary.translate(wordInEnglish);
		
	}

}
