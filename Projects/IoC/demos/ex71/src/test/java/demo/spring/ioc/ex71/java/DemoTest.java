/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex71.java;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * A unit test for the Java-based configuration.
 * 
 * @author Franz Tost
 *
 */
public class DemoTest {
	
	// tests
	// ........................................................................

	@Test public void test() {
		
		try (AnnotationConfigApplicationContext context =
		    new AnnotationConfigApplicationContext(DemoConfig.class)
		) {
		
			Translator translator = (Translator) context.getBean("translator");
			
			assertNotNull(translator);
			assertEquals("lluvia", translator.translate("rain"));
			assertEquals("sol",    translator.translate("sun"));
			assertEquals("viento", translator.translate("wind"));
			assertEquals("nieve",  translator.translate("snow"));
			
		} // try-with-resources

	}

}
