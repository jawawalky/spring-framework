/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex71.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import demo.spring.ioc.ex71.xml.Translator;

/**
 * A unit test for the XML-based configuration.
 * 
 * @author Franz Tost
 *
 */
public class DemoTest {

	// tests
	// ........................................................................

	@Test public void test() {
		
		try (ClassPathXmlApplicationContext context =
		    new ClassPathXmlApplicationContext("demo.xml")) {
		
			Translator translator = (Translator) context.getBean("translator");
			
			assertNotNull(translator);
			assertEquals("Regen",  translator.translate("rain"));
			assertEquals("Sonne",  translator.translate("sun"));
			assertEquals("Wind",   translator.translate("wind"));
			assertEquals("Schnee", translator.translate("snow"));
			
		} // try-with-resources
		
	}

}
