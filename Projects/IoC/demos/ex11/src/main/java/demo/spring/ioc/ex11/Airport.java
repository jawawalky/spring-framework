/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex11;

/**
 * A JavaBean modeling an airport.
 * 
 * @author Franz Tost
 *
 */
public class Airport {

	// fields
	// ........................................................................

	private String code;

	private String name;

	private String city;

	// constructors
	// ........................................................................

	public Airport() {

		super();

	}

	public Airport(String code, String name, String city) {

		super();

		this.code = code;
		this.name = name;
		this.city = city;

	}

	// methods
	// ........................................................................

	public String getCode() {

		return code;

	}

	public void setCode(String code) {

		this.code = code;

	}

	public String getName() {

		return name;

	}

	public void setName(String name) {

		this.name = name;

	}

	public String getCity() {

		return city;

	}

	public void setCity(String city) {

		this.city = city;

	}

	public String toString() {

		return this.getName() + " (" + this.getCode() + ")";

	}

}
