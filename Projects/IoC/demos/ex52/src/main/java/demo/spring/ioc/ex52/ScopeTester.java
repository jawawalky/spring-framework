/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex52;

import org.springframework.context.ApplicationContext;

import demo.util.Demo;

/**
 * @author Franz Tost
 *
 */
public class ScopeTester extends Thread {
	
	// fields
	// ........................................................................
	
	private ApplicationContext context;

	// constructors
	// ........................................................................
	
	public ScopeTester(ApplicationContext context) {
		
		super();
		
		this.context = context;
		
	}
	
	// methods
	// ........................................................................
	
	@Override
	public void run() {
		
		for (int i = 0; i < 5; i++) {
			
			Bean bean = (Bean) this.context.getBean("bean");
			Demo.println(this.getName() + ": " + bean.toString());
			Demo.sleep(200, 500);
			
		} // for
		
	}

}
