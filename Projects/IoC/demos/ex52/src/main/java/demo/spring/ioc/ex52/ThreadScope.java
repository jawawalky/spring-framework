/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex52;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.config.Scope;

/**
 * A per-thread-scope.
 * 
 * @author Franz Tost
 *
 */
public class ThreadScope implements Scope {
	
	// fields
	// ........................................................................
	
	private ThreadLocal<Map<String, Object>> threadContext = new ThreadLocal<Map<String,Object>>();

	// methods
	// ........................................................................

	@Override
	public Object get(String name, ObjectFactory<?> objectFactory) {
		
		Map<String, Object> values = this.getValues();
		
		Object value = values.get(name);
		if (value == null) {
			
			value = objectFactory.getObject();
			values.put(name, value);
			
		} // if
		return value;
		
	}

	@Override
	public String getConversationId() {
		
		return String.valueOf(Thread.currentThread().getId());
		
	}

	@Override
	public void registerDestructionCallback(String name, Runnable callback) {
		
		// Not supported

	}

	@Override
	public Object remove(String name) {
		
		return this.getValues().remove(name);
		
	}

	@Override
	public Object resolveContextualObject(String name) {
		
		return this.getValues().get(name);
		
	}
	
	private Map<String, Object> getValues() {
		
		Map<String, Object> values = this.threadContext.get();
		if (values == null) {
			
			values = new HashMap<String, Object>();
			this.threadContext.set(values);
			
		} // if
		return values;
		
	}

}
