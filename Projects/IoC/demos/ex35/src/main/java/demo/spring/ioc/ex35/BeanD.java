/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex35;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * An example bean, which is life cycle aware by configuration.
 * <p/>
 * <b>Note:</b>
 * <blockquote>
 * 	No Spring import are needed in this class.
 * </blockquote>
 * 
 * @author Franz Tost
 *
 */
public class BeanD extends Bean0 {
	
	// methods
	// ........................................................................

	@PostConstruct
	public void init() {
		
		this.print("POST CONSTRUCT");
		
	}

	@PreDestroy
	public void exit() {
		
		this.print("PRE DESTROY");
		
	}

}
