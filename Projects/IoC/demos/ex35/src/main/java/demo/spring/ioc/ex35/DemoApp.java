/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex35;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import demo.util.Demo;

/**
 * This demo teaches you how to use lookup method injection.
 * 
 * @author Franz Tost
 *
 */
public class DemoApp {

	// constructors /////

	private DemoApp() { }

	
	// methods /////

	private void runDemo() {

		Demo.log("Running demo ...");
		
		try (ClassPathXmlApplicationContext context =
		    new ClassPathXmlApplicationContext("beans.xml")) {
		
			context.getBean("beanA");
			context.getBean("beanB");
			context.getBean("beanC");
			context.getBean("beanD");
			
			Demo.log("Finished.");
			
		} // try
		
	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new DemoApp().runDemo();

	}

}
