/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex35;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

/**
 * An example bean using the life cycle interfaces
 * <code>InitializingBean</code> and <code>DisposableBean</code>.
 * 
 * @author Franz Tost
 *
 */
public class BeanA extends Bean0 implements InitializingBean, DisposableBean {
	
	// methods
	// ........................................................................

	@Override
	public void afterPropertiesSet() throws Exception {
		
		this.print("AFTER PROPERTIES SET");
		
	}

	@Override
	public void destroy() throws Exception {
		
		this.print("DESTROY");
		
	}

}
