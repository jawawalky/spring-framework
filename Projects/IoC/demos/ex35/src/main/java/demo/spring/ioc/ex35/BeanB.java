/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex35;

/**
 * An example bean, which uses life cycle annotations.
 * 
 * @author Franz Tost
 *
 */
public class BeanB extends Bean0 {
	
	// methods
	// ........................................................................

	public void init() {
		
		this.print("INIT");
		
	}

	public void exit() {
		
		this.print("EXIT");
		
	}

}
