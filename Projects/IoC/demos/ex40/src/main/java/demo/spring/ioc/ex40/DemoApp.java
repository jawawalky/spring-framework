/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex40;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import demo.util.Demo;

/**
 * This demo teaches you how to use Java-based configurations.
 * 
 * @author Franz Tost
 *
 */
public class DemoApp {

	// constructors
	// ........................................................................

	private DemoApp() { }

	// methods
	// ........................................................................

	private void runDemo() {

		Demo.log("Running demo ...");
		
		try (
		    AnnotationConfigApplicationContext context =
		        new AnnotationConfigApplicationContext(DemoConfig.class);
		) {
			
			Service service = context.getBean(Service.class);
			service.doService();
			
		} // try

		Demo.log("Finished.");

	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new DemoApp().runDemo();

	}

}
