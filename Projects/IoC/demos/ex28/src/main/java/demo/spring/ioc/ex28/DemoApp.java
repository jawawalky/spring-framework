/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex28;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import demo.spring.ioc.ex28.util.Translator;
import demo.util.Demo;

/**
 * This demo teaches you how components can be auto-detected by the bean
 * container.
 * 
 * @author Franz Tost
 *
 */
public class DemoApp {

	// constructors
	// ........................................................................

	private DemoApp() { }

	// methods
	// ........................................................................

	private void runDemo() {

		Demo.log("Running demo ...");
		
		try (
			ClassPathXmlApplicationContext context =
			    new ClassPathXmlApplicationContext("beans.xml");
		) {
			
			Translator translator = context.getBean(Translator.class);
			
			translator.translate("rain");
			translator.translate("sun");
			translator.translate("snow");
			translator.translate("wind");
			
			Demo.log("Finished.");
			
		} // try

	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new DemoApp().runDemo();

	}

}
