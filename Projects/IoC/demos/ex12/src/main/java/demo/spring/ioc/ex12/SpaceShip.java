/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex12;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * A dummy class for some complex machine.
 * 
 * @author Franz Tost
 *
 */
public class SpaceShip {
	
	// fields
	// ........................................................................
	
	private Properties settings = new Properties();
	
	private Set<String> crew = new HashSet<String>();
	
	private List<String> decks = new ArrayList<String>();
	
	private Map<String, String> tasks = new HashMap<String, String>();

	// methods
	// ........................................................................
	
	public Properties getSettings() {
		
		return this.settings;
		
	}

	public void setSettings(Properties options) {
		
		this.settings = options;
		
	}

	public Set<String> getCrew() {
		
		return this.crew;
		
	}

	public void setCrew(Set<String> crew) {
		
		this.crew = crew;
		
	}

	public List<String> getDecks() {
		
		return this.decks;
		
	}

	public void setDecks(List<String> decks) {
		
		this.decks = decks;
		
	}

	public Map<String, String> getTasks() {
		
		return this.tasks;
		
	}

	public void setTasks(Map<String, String> tasks) {
		
		this.tasks = tasks;
		
	}
	
}
