/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex29;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;


/**
 * A dictionary, which translates English words into German words.
 * 
 * @author Franz Tost
 *
 */
@Component
public class GermanDictionary implements Dictionary {
	
	// fields
	// ........................................................................
	
	private Map<String, String> words = new HashMap<String, String>();
	
	// constructor
	// ........................................................................
	
	public GermanDictionary() {
		
		super();
		
		this.words.put("rain", "Regen");
		this.words.put("wind", "Wind");
		this.words.put("sun",  "Sonne");
		this.words.put("snow", "Schnee");
		
	}

	// methods
	// ........................................................................

	@Override
	public String translate(String word) {
		
		return this.words.get(word);
		
	}

}
