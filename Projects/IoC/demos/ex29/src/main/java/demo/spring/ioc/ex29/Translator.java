/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ioc.ex29;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import demo.util.Demo;

/**
 * A translator object.
 * 
 * @author Franz Tost
 *
 */
@Component
public class Translator {
	
	// fields
	// ........................................................................
	
	private Dictionary dictionary;

	// constructors
	// ........................................................................
	
	@Autowired
	public Translator(Dictionary dictionary) {
		
		super();
		
		this.dictionary = dictionary;
		
	}

	// methods
	// ........................................................................
	
	public void translate(String word) {
		
		Demo.log(word + " --> " + this.dictionary.translate(word));
		
	}

}
