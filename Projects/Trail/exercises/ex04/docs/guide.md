# Annotation-based Bean Definition

## How to Define a Bean

If we use Java-based configurations instead of XML-based configurations, then
no XML configuration file is needed. Instead of triggering component scanning
by

```
<context:component-scan base-package="demo.spring.ex04"/>
```

in the XML file, we add the annotation `@ComponentScan` to out *Java*
configuration class.
