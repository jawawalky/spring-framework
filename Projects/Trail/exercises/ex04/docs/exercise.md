# Annotation-based Bean Definitions

## Activating Component Scan

To make *Spring* scan for components, we need to activate component scan in our
Java configuration file.

1. Create a new *Java* class called
   `demo.spring.ex04.ApplicationConfiguration`.
1. Make this class a configuration class.
1. Activate component scan for the package `demo.spring.ex04` on that class.

## Accessing Beans

1. Open [Main.java](../src/main/java/demo/spring/ex04/Main.java).
1. Follow the instructions of the *TODO* sections.
