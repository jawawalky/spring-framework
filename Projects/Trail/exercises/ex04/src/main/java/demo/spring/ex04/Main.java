/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex04;

import demo.util.Demo;

/**
 * This demo teaches you, how you can define beans by annotating classes
 * with certain annotations such as {@code Component}.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors /////

	private Main() { }
	

	// methods /////

	private void runDemo() {

		Demo.log("Running demo ...");
		
		// TODO
		//
		//  o Create the application context, which reads the configuration
		//    'ApplicationConfiguration'.
		//
		//  o Get the 'Dictionary' bean from the context.
		//
		//  o Translate the words
		//
		//      o 'rain'
		//      o 'sun'
		//      o 'snow'
		//      o 'wind'
		//
		
		Demo.log("Finished.");

	}
	
	private void printTranslation(
		final Dictionary dictionary,
		final String     wordInEnglish
	) {
		
		Demo.log(wordInEnglish + " --> " + dictionary.translate(wordInEnglish));
		
	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Main().runDemo();

	}

}
