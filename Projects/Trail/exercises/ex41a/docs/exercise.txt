JMS Exporter
============

A) Configuring the Exporter
===========================

  'beans.xml':
  ------------

  1) Add JEE lookups for the connection factory and the message queue.
  
     Hints:
     
       o Use the syntax '<jee:jndi-lookup id="???" jndi-name="???"/>'
         for JNDI lookups.
         
       o The JNDI name of the connection factory is 'ConnectionFactory'.
         Bind it to the ID 'connectionFactory'.
         
       o The JNDI name of the queue is 'queue/demo'. Bind it to the ID 'queue'.
       
       
  2) Add a JMS exporter bean of type
     'org.springframework.jms.remoting.JmsInvokerServiceExporter' and
     give it the ID 'exporter'.
     
  3) Set its property 'service' to the exported service bean.
  
  4) Set its property 'serviceInterface' to the interface of
     exported service.
     
     
  5) Add an anonymous bean of type
     'org.springframework.jms.listener.SimpleMessageListenerContainer'.
     
  6) Set its properties
  
       o 'connectionFactory'      JMS connection factory
       o 'destination'            JMS Queue
       o 'concurrentConsumers'    = 3
       o 'messageListener'        Exporter bean
