/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2018 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex39;

import org.springframework.context.annotation.Configuration;

/**
 * Java configuration of the client application.
 * 
 * @author Franz Tost
 *
 */
@SuppressWarnings("deprecation")
@Configuration
public class ClientConfig {
	
	// beans /////
	
	// TODO
	//
	//  o Create a RMI proxy bean for the address book.
	//
	
}
