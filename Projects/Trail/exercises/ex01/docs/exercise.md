# XML-based Configuration

## Defining Beans

Currently we only have a single bean class, which is
[Airport](../src/main/java/demo/spring/ex01/Airport.java).
We would like to create different beans for the airports of

- Frankfurt
- Barcelona
- Madrid
- Lisbon

The configuration files for the different airports are

- [germany.xml](../src/main/resources/germany.xml)
- [spain.xml](../src/main/resources/spain.xml)
- [airports.xml](../src/main/resources/airports.xml)

Define the bean instances by

1. Open a XML-configuration file.
1. Follow the instructions of the *TODO* sections.

## Accessing Beans

In our [application](../src/main/java/demo/spring/ex01/Main.java) we need to
define a context, which will contain our beans

1. Open [Main.java](../src/main/java/demo/spring/ex01/Main.java).
1. Follow the instructions of the *TODO* sections.
