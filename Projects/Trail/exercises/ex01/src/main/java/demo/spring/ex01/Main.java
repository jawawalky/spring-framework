/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex01;

import demo.util.Demo;

/**
 * This is a simple demo, which shows you how you can configure your
 * beans with XML-based configuration files.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors /////

	private Main() { }
	

	// methods /////

	private void runDemo() {

		Demo.log("Running demo ...");
		
		// TODO
		//
		//  o Create an instance of 'ClassPathXmlApplicationContext', which
		//    contains all bean definitions of all three XML configuration
		//    files.
		//
		//    Hint: 'ClassPathXmlApplicationContext' is 'AutoCloseable', so use
		//          it with a 'try-with-resources' construction.
		//
		//  o Retrieve all defined airports and print them on the console.
		//
		
		Demo.log("Finished.");

	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Main().runDemo();

	}

}
