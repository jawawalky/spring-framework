/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2018 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex35.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.object.MappingSqlQuery;
import org.springframework.stereotype.Component;

import demo.spring.ex35.model.Contact;

/**
 * A query object, which reads all contacts.
 * 
 * @author Franz Tost
 *
 */
@Component
public class AllContacts extends MappingSqlQuery<Contact> {

	// constructors /////

	@Autowired
	public AllContacts(final DataSource dataSource) {

		super(dataSource, "SELECT * FROM Addressbook");
		this.compile();

	}

	
	// methods /////

	@Override
	public Contact mapRow(
		final ResultSet resultSet,
		final int       rowIndex
	) throws SQLException {

		return new Contact(
			resultSet.getLong("id"),
			resultSet.getString("name"),
			resultSet.getString("street"),
			resultSet.getInt("houseNo"),
			resultSet.getString("zipCode"),
			resultSet.getString("city")
		);

	}

}
