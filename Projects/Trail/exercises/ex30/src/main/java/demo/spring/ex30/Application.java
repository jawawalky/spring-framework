/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2021 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex30;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Component;

import demo.util.Demo;

/**
 * An example for using the {@code JdbcTemplate}.
 * 
 * @author Franz Tost
 *
 */
@Configuration
@Component
public class Application {

	// fields /////
	
	// TODO
	//
	//  o Autowire a 'JdbcTemplate' and call it 'jdbcTemplate'.
	//

	
	// beans /////
	
	@Bean
	public DataSource dataSource() {
		
		try {
			
			Class.forName("org.hsqldb.jdbcDriver");
			return new DriverManagerDataSource("jdbc:hsqldb:mem:demo", "sa", null);
			
		} // try
		catch (ClassNotFoundException e) {
			
			throw new RuntimeException(e);
			
		} // catch
		
	}
	
	// TODO
	//
	//  o Configure a Spring bean of type 'JdbcTemplate', which operates
	//    on the defined data source.
	//
	
	
	// methods /////
	
	private void runDemo() {

		Demo.log("Running demo ...");
		
		this.setupSchema();
		
		this.createContact();
		this.readContact();
		this.updateContact();
		this.readContact();
		this.deleteContact();
		this.readContact();
		
		Demo.log("Finished.");

	}
	
    private void setupSchema() {
		
        Demo.log("Creating schema ...");
        
        // TODO
        //
        //  o Use the JdbcTemplate to set up the required database schema
        //
        //    Hint: 
        //
        //      CREATE TABLE Addressbook (
        //          id INTEGER,
        //          name VARCHAR(50),
        //          street VARCHAR(100),
        //          houseNo INTEGER,
        //          zipCode VARCHAR(10),
        //          city VARCHAR(30)
        //      )
        //
	    
	}
	
    private void createContact() {
		
        Demo.log("Creating contact ...");
        
        // TODO
        //
        //  o Use the JdbcTemplate to insert the following record into
        //    the table 'Addressbook'
        //
        //      2L, 'Prime Minister', 'Downing Street', 11, 'SW1A 2AA', 'London'
        //
        //    Hint: 
        //
        //      INSERT INTO Addressbook VALUES (?, ?, ?, ?, ?, ?)
        //

	}

	private void readContact() {
		
		Demo.log("Reading contact ...");
		
        // TODO
        //
        //  o Use the JdbcTemplate to perform a query, which reads the record
		//    with the ID '2L'.
        //
        //    Hint: 
        //
        //      SELECT * FROM Addressbook WHERE ID = ?
        //
		
		List<Map<String, Object>> rows = null;
		
		if (rows.isEmpty()) {
			
			Demo.log("---");
			return;
			
		} // if
		
        Map<String, Object> row = rows.get(0);
        
        StringBuilder stringBuilder = new StringBuilder();
        
        Iterator<Map.Entry<String, Object>> iterator = row.entrySet().iterator();
        
        while (iterator.hasNext()) {
        	
            Map.Entry<String, Object> entry = iterator.next();
            stringBuilder.append(entry.getKey());
            stringBuilder.append("=");
            stringBuilder.append(entry.getValue());
            stringBuilder.append(iterator.hasNext() ? ", " : "");
            
        } // while
			
        Demo.log(stringBuilder.toString());
		
	}

    private void updateContact() {
		
        Demo.log("Updating contact ...");
        
        // TODO
        //
        //  o Use the JdbcTemplate to update the record with the ID '2L' with
        //    the correct house number 10 instead of 11.
        //
        //    Hint: 
        //
        //      UPDATE Addressbook SET houseNo = ? WHERE id = ?
        //

	}

    private void deleteContact() {
    	
        Demo.log("Deleting contact ...");
        
        // TODO
        //
        //  o Use the JdbcTemplate to delete the record with the ID '2L'.
        //
        //    Hint: 
        //
        //      DELETE FROM Addressbook WHERE id = ?
        //

	}

	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {
		
		try (
			AnnotationConfigApplicationContext context =
			    new AnnotationConfigApplicationContext(Application.class);
		) {
			
			context.getBean(Application.class).runDemo();
	
		} // try
		
	}

}
