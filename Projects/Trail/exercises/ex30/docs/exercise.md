# JdbcTemplate

## Why we Use `JdbcTemplate`

- A `JdbcTemplate` hides all *JDBC* standard code. We only need to specifiy
  a *SQL* command and its parameters.
- Database code becomes leaner and cleaner.
	 
	 
## What is Your Exercise?

1. Open [Application](../src/main/java/demo/spring/ex30/Application.java).
1. Configure a `JdbcTemplate`.
1. Use the `JdbcTemplate` for all database operations. (The required *SQL*
   commands are provided in the *TODO* sections).
