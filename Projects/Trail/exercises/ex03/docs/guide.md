# Annotation-based Bean Definition

## How to Define a Bean

Often only one instance is needed of a certain bean class. Such beans are
usually referenced by their type or the type of an interface that they
implement, instead of referencing them by their name.

We can annotate such bean classes with `@Component`. *Spring* registers
an instance of such a class in the application context. To make this happen,
we need to add the following element

```
<context:component-scan base-package="demo.spring.ex03"/>
```

to our XML configuration file. The `base-package` tells *Spring*, which
packages it should scan for components.


## How to Access a Bean

Beans defined by component classes are usually accessed by their type or
by an interface that they implement. That only works, if there is no other
bean of the same type or interface.

**Example:**

```
try (
	ClassPathXmlApplicationContext context =
	    new ClassPathXmlApplicationContext("beans.xml");
) {

	Dictionary dictionary = context.getBean(Dictionary.class);
	...

} // try
```
