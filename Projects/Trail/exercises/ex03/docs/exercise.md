# Annotation-based Bean Definitions

## Defining Components

Defining a component is straight forward. Just put `@Component` before
the declaration of the class.

1. Open [GermanDictionary.java](../src/main/java/demo/spring/ex03/GermanDictionary.java).
1. Follow the instructions of the *TODO* sections.

## Activating Component Scan

To make *Spring* scan for components, we need to activate component scan in our
XML configuration file.

1. Open [beans.xml](../src/main/resources/beans.xml).
1. Follow the instructions of the *TODO* sections.

## Accessing Beans

In our [application](../src/main/java/demo/spring/ex03/Main.java) we need to
define a context, which will contain our beans

1. Open [Main.java](../src/main/java/demo/spring/ex03/Main.java).
1. Follow the instructions of the *TODO* sections.
