/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex10;

import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import demo.util.Demo;

/**
 * In this demo you learn, how to use events.
 * 
 * @author Franz Tost
 *
 */
@Component
@Configuration
public class Application {
	
	// constructors /////

	Application() { }
	

	// methods /////

	@Bean
	public Scores scores() {
		
		return new Scores();
		
	}
	
	@Bean
	@Scope(SCOPE_PROTOTYPE)
	public Player player() {
		
		return new Player();
		
	}
	
	private void runDemo(final AnnotationConfigApplicationContext context) {

		Demo.log("Running application ...");
		
		// TODO
		//
		//  o Create a player 'alice' and a player 'duke'.
		//
		//  o Set their names.
		//
		//  o Let them play a few games.
		//
		//  o Let Duke win some of the games and Alice the others.
		//
		//  o Finally show the scores.
		//
		
		Demo.log("Finished.");

	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		try (
			AnnotationConfigApplicationContext context =
			    new AnnotationConfigApplicationContext(
			    	Application.class
			    );
		) {
			
			final Application application = context.getBean(Application.class);
			application.runDemo(context);
	
		} // try
			
	}

}
