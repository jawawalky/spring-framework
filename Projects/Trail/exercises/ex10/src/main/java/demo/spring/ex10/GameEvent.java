/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex10;

/**
 * An event fired, when a game is over.
 * 
 * @author Franz Tost
 *
 */
public class GameEvent {
	
	// TODO
	//
	//  o Create an event class, which transports the name of the winner
	//    of a game.
	//

}
