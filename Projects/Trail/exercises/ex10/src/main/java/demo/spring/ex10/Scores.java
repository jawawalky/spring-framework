/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex10;

import java.util.HashMap;
import java.util.Map;

import demo.util.Demo;

/**
 * A bean, which counts the played games and stores its winners.
 * 
 * @author Franz Tost
 *
 */
public class Scores {
	
	// fields /////
	
	private int gameCount;
	
	private Map<String, Integer> winners = new HashMap<>();
	
	
	// methods /////
	
	public int getGameCount() {
		
		return this.gameCount;
		
	}
	

	// TODO
	//
	//  o Listen for 'GameEvent's.
	//
	//  o When a 'GameEvent' arrives, then increase the game count and
	//    the win count of the player, who won the game.
	//
	//    Hint: Increase the counter in the map 'winners' for the player
	//          who won the game.
	//

	public void show() {
		
		this.winners
			.entrySet()
			.stream()
			.forEach(e -> Demo.log(
				"%s has won %d out of %d games.",
				e.getKey(),
				e.getValue(),
				this.gameCount
			));
		
	}

}
