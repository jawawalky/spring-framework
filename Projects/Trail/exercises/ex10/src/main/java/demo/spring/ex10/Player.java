/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex10;

/**
 * A player, who sends an event, when he or she wins a game.
 * 
 * @author Franz Tost
 *
 */
public class Player {
	
	// fields /////
	
	private String name;
	
	
	// methods /////
	
	public void setName(final String name) {
		
		this.name = name;
		
	}
	
	public void wins() {
		
		// TODO
		//
		//  o If a player wins a game, publish a 'GameEvent' with the name
		//    of the player.
		//
		
	}
	
}
