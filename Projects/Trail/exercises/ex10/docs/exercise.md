# Events

## Your Exercise

Two players, Duke and Alice
([Player](../src/main/java/demo/spring/ex10/Player.java), play a game.
Sometimes Duke wins and sometimes Alice wins. The scores will be denoted
on a score board
([Scores](../src/main/java/demo/spring/ex10/Scores.java). It contains
the number of played games and for each player the number of won games.

In this exercise the `Player` beans should only be loosely coupled with
the `Scores` bean by an event mechanism. When a `Player` wins a game, then
an event will be published by the `Player`. The `Scores` receives that event
and sets its internal values accordingly.

1. Read the ([Guide](guide.md) carefully.
1. Apply the event concepts learned to the exercise.
1. Work on the classes in the following order
    1. [GameEvent](../src/main/java/demo/spring/ex10/GameEvent.java)
    1. [Player](../src/main/java/demo/spring/ex10/Player.java)
    1. [Scores](../src/main/java/demo/spring/ex10/Scores.java)
    1. [Application](../src/main/java/demo/spring/ex10/Application.java)
