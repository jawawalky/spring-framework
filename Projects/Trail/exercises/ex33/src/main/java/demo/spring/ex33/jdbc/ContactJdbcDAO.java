/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2018 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex33.jdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * A DAO interface for {@code Contact}s.
 * 
 * @author Franz Tost
 * 
 * TODO
 * 
 *  o Implement the DAO interface 'ContactDAO'.
 */
@Component
public class ContactJdbcDAO {

	// fields /////
	
	@Autowired
	private ContactCreator contactCreator;
	
	@Autowired
	private ContactReader  contactReader;
	
	@Autowired
	private ContactUpdater contactUpdater;
	
	@Autowired
	private ContactDeleter contactDeleter;

	
	// methods /////
	
	// TODO
	//
	//  o Implement the interface methods with the help of the of
	//    the 'SqlObjects'.
	//
	
}
