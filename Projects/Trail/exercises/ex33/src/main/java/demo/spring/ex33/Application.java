/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2018 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex33;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Component;

import demo.spring.ex33.dao.ContactDAO;
import demo.spring.ex33.model.Contact;
import demo.util.Demo;

/**
 * An example for using DAO objects.
 * 
 * @author Franz Tost
 *
 */
@Configuration
@Component
@ComponentScan("demo.spring.ex33.jdbc")
public class Application {

	// fields /////

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private ContactDAO contactDAO;
	
	
	// beans /////
	
	@Bean
	public DataSource dataSource() {
		
		try {
			
			Class.forName("org.hsqldb.jdbcDriver");
			return new DriverManagerDataSource("jdbc:hsqldb:mem:demo", "sa", null);
			
		} // try
		catch (ClassNotFoundException e) {
			
			throw new RuntimeException(e);
			
		} // catch
		
	}
	
	@Bean
	public JdbcTemplate jdbcTemplate() {
		
		return new JdbcTemplate(this.dataSource());
		
	}

	
	// methods /////

	private void runDemo() {

		Demo.log("Running demo ...");
		
		this.setupSchema();
		
		Contact contact =
			new Contact(2L, "Prime Minister", "Downing Street", 11, "SW1A 2AA", "London");
		
		// TODO
		//
		//  o Create a contact, using the DAO bean.
		//
		//  o Read that contact, using the DAO bean and print its
		//    values on the console.
		//
		//  o Update the contact with the correct house number, using
		//    the DAO bean.
		//
		//  o Read that contact, using the DAO bean and print its
		//    values on the console.
		//
		//  o Delete the contact, using the DAO bean.
		//

		Demo.log("Finished.");

	}
	
    private void setupSchema() {
		
        Demo.log("Creating schema ...");
		
        this.jdbcTemplate.update(
            "CREATE TABLE Addressbook (" +
            "id INTEGER, "               +
            "name VARCHAR(50), "         +
            "street VARCHAR(100), "      +
            "houseNo INTEGER, "          +
            "zipCode VARCHAR(10), "      +
            "city VARCHAR(30))"
        );
	    
	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {
		
		try (
			AnnotationConfigApplicationContext context =
			    new AnnotationConfigApplicationContext(Application.class);
		) {
			
			context.getBean(Application.class).runDemo();
	
		} // try
		
	}

}
