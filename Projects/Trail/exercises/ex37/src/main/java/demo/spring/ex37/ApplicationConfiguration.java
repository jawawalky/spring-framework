/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2018 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex37;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.transaction.PlatformTransactionManager;

import demo.spring.ex37.dao.ContactDAO;
import demo.spring.ex37.dao.DaoAddressBook;
import demo.spring.ex37.jdbc.ContactJdbcDAO;
import demo.spring.ex37.service.AddressBook;

/**
 * The application configuration.
 * 
 * @author Franz Tost
 * 
 * TODO
 * 
 *  o Enable declarative transaction management.
 */
@Configuration
public class ApplicationConfiguration {

	// beans /////
	
	@Bean
	public DataSource dataSource() {
		
		try {
			
			Class.forName("org.hsqldb.jdbcDriver");
			return new DriverManagerDataSource("jdbc:hsqldb:mem:demo", "sa", null);
			
		} // try
		catch (ClassNotFoundException e) {
			
			throw new RuntimeException(e);
			
		} // catch
		
	}
	
	@Bean
	public PlatformTransactionManager transactionManager() {
		
		return new DataSourceTransactionManager(this.dataSource());
		
	}
	
	@Bean
	public ContactDAO contactDAO() {
		
		return new ContactJdbcDAO(this.dataSource());
		
	}
	
	@Bean
	public AddressBook addressBook() {
		
		return new DaoAddressBook(this.contactDAO());
		
	}
	
	@Bean
	public JdbcTemplate jdbcTemplate() {
		
		return new JdbcTemplate(this.dataSource());
		
	}
	
}
