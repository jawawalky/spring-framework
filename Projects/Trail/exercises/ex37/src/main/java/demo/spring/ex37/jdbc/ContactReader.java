/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2018 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex37.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import javax.sql.DataSource;

import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.MappingSqlQuery;

import demo.spring.ex37.model.Contact;

/**
 * A query object, which reads the contact with the specified ID.
 * 
 * @author Franz Tost
 *
 */
public class ContactReader extends MappingSqlQuery<Contact> {

	// constructors
	// ........................................................................

	public ContactReader(final DataSource dataSource) {

		super(dataSource, "SELECT * FROM Addressbook WHERE id = ?");
		this.declareParameter(new SqlParameter("id", Types.INTEGER));
		this.compile();

	}

	// methods
	// ........................................................................

	@Override public Contact mapRow(
		final ResultSet resultSet,
		final int       rowIndex
	) throws SQLException {

		return new Contact(
			resultSet.getLong("id"),
			resultSet.getString("name"),
			resultSet.getString("street"),
			resultSet.getInt("houseNo"),
			resultSet.getString("zipCode"),
			resultSet.getString("city")
		);

	}

	public Contact read(final Long id) { return this.findObject(id); }
	
}
