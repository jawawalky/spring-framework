/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2018 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex32;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

/**
 * A query object, which reads the contact with the specified ID.
 * 
 * @author Franz Tost
 * 
 * TODO
 * 
 *   o Let the class extend the base class 'MappingSqlQuery'.
 *   
 *   o Let this class be a component.
 */
public class ContactReader {

	// constructors /////
	
	// TODO
	//
	//  o Autowire the 'DataSource'.
	//

	public ContactReader(final DataSource dataSource) {
		
		// TODO
		//
		//  o Use the constructor of the super-class to set the data source
		//    and the prepared SQL statement.
		//
		//  o Declare the parameter 'id'.
		//
		//  o 'Compile' the query.
		//

	}

	
	// methods /////

	public Contact mapRow(
		final ResultSet resultSet,
		final int       rowIndex
	) throws SQLException {

		return new Contact(
			resultSet.getLong("id"),
			resultSet.getString("name"),
			resultSet.getString("street"),
			resultSet.getInt("houseNo"),
			resultSet.getString("zipCode"),
			resultSet.getString("city")
		);

	}

	public Contact read(final Long id) {
		
		// TODO
		//
		//  o Use a method of the 'MappingSqlQuery' to query a contact.
		//
		
		return null;
		
	}
	
}
