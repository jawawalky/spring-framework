/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2018 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex32;

import javax.sql.DataSource;

/**
 * A SQL object, which deletes a contact.
 * 
 * @author Franz Tost
 * 
 * TODO
 * 
 *   o Let the class extend the base class 'SqlUpdate'.
 *   
 *   o Let this class be a component.
 */
public class ContactDeleter {

	// constructors /////
	
	// TODO
	//
	//  o Autowire the 'DataSource'.
	//

	public ContactDeleter(final DataSource dataSource) {
		
		// TODO
		//
		//  o Use the constructor of the super-class to set the data source
		//    and the prepared SQL statement.
		//
		//  o Declare the parameter 'id'.
		//

	}

	
	// methods /////

	public int delete(final Long id) {
		
		// TODO
		//
		//  o Use a method of the 'SqlUpdate' to create a contact.
		//
		
		return 0;

	}

}
