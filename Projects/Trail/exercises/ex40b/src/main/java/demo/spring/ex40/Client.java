/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2018 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex40;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import demo.spring.ex40.model.Contact;
import demo.spring.ex40.service.AddressBook;
import demo.util.Demo;

/**
 * An example for a client communicating via HTTP with a server.
 * 
 * @author Franz Tost
 *
 */
public class Client {

	// constructors /////

	private Client() { }

	
	// methods /////

	private void run() {

		Demo.log("Running demo ...");
		
		try (
			ClassPathXmlApplicationContext context =
			    new ClassPathXmlApplicationContext("beans.xml");
		) {
			
			final AddressBook addressBook = context.getBean(AddressBook.class);
	
			Contact contact =
				new Contact("Prime Minister", "Downing Street", 11, "SW1A 2AA", "London");
				
			Demo.log("Creating contact ...");
			final Long id = addressBook.newContact(contact);
			
			Demo.log("Reading contact ...");
			contact = addressBook.getContact(id);
			Demo.log("Contact: %s", contact.toString());
			
			Demo.log("Updating contact ...");
			contact.setHouseNo(10);
			addressBook.updateContact(contact);

			Demo.log("Reading contact ...");
			contact = addressBook.getContact(id);
			Demo.log("Contact: %s", contact.toString());

			Demo.log("Deleting contact ...");
			addressBook.removeContact(id);
			
			Demo.log("Reading contact ...");
			contact = addressBook.getContact(id);
			Demo.log("Contact deleted: %s", (contact == null));
			
		} // try
			
		Demo.log("Finished.");

	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {
		
		new Client().run();
		
	}

}
