# Java-based Configuration

## Defining Beans

The configuration files for the different airports are

- [Germany.java](../src/main/java/demo/spring/ex02/Germany.java)
- [Spain.java](../src/main/java/demo/spring/ex02/Spain.java)
- [Portugal.java](../src/main/java/demo/spring/ex02/Portugal.java)

Define the bean instances by

1. Open a *Java* configuration file.
1. Follow the instructions of the *TODO* sections.

## Accessing Beans

In our [application](../src/main/java/demo/spring/ex02/Main.java) we need to
define a context, which will contain our beans

1. Open [Main.java](../src/main/java/demo/spring/ex02/Main.java).
1. Follow the instructions of the *TODO* sections.
