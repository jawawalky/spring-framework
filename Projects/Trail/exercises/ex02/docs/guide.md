# Java-based Configuration

## How to Define a Bean

When we use Java-based configurations, then we do not need XML configuration
files. Our beans can be defined in regular *Java* classes. *Java* classes
containing bean definitions must be annotated with `@Configuration`. Beans
are defined by methods of the configuration class, which return the bean
objects. A bean generating method must be annotated by `@Bean`. By default
the name of the bean is the name of the generating method.

**Example:**

```
@Configuration
public class AirportConfiguration {

	@Bean
	public Airport fra() {
		
		return new Airport(
			"FRA",
			"Frankfurt International Airport",
			"Frankfurt am Main"
		);

	}

}
```

## How to Access a Bean

Instead of using `ClassPathXmlApplicationContext` in case of XML-based
configurations, we use `AnnotationConfigApplicationContext` for Java-based
configurations. When we create the application context, we pass the *Java*
configuration classes to the constructor of the context.

**Example:**

```
try (
	AnnotationConfigApplicationContext context =
	    new AnnotationConfigApplicationContext(
	   		AirportConfiguration.class
	   	);
) {

	Airport airport = (Airport) context.getBean("fra");
	...

} // try
```

> The access to a bean by `getBean(String)` is independent of the chosen
> application context and configuration type!
