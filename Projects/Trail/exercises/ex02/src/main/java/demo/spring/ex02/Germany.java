/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex02;

/**
 * A Java class containing the bean definitions for airports in Germany.
 * 
 * @author Franz Tost
 * 
 * TODO
 * 
 *  o make this class a configuration class.
 */
public class Germany {

	// methods /////
	
	// TODO
	//
	//  o Define a bean for the Frankfurt airport with the bean name 'fra'.
	//

}
