/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex02;

/**
 * A Java class containing the bean definitions for airports in Portugal.
 * 
 * @author Franz Tost
 * 
 * TODO
 * 
 *  o make this class a configuration class.
 */
public class Portugal {

	// methods /////

	// TODO
	//
	//  o Define a bean for the Lisbon airport with the bean name 'lis'.
	//

}
