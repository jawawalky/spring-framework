# Qualifiers

## Defining Qualifier Annotations

1. Define two new qualifier annotations in the package
   `demo.sping.ex06`.
1. One with the name `German`.
1. And one with the name `Spanish`.

## Solving Ambiguities

1. Use the qualifier annotations `@German` and `@Spanish` to solve ambiguities
   among components.
1. Use the qualifier annotations `@German` and `@Spanish` to solve ambiguities
   on injection points.

## Running Application

1. Open the [application](../src/main/java/demo/spring/ex06/Main.java).
1. Perform some translations from English to German.
   (See the instructions of the *TODO* section.)
1. Perform some translations from English to Spanish.
   (See the instructions of the *TODO* section.)
   