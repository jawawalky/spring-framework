/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex06;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import demo.util.Demo;

/**
 * In this demo you learn, how to use qualifiers to avoid ambiguity.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors /////

	private Main() { }
	

	// methods /////

	private void runDemo() {

		Demo.log("Running demo ...");
		
		try (
			AnnotationConfigApplicationContext context =
			    new AnnotationConfigApplicationContext(
			    	ApplicationConfiguration.class
			    );
		) {

			Demo.log("Translations to German ...");
			
			// TODO
			//
			//  o Get a German translator and translate some words.
			//
			
			Demo.log("Translations to Spanish ...");
			
			// TODO
			//
			//  o Get a Spanish translator and translate some words.
			//
			
		} // try
		
		Demo.log("Finished.");

	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Main().runDemo();

	}

}
