/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex06;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

/**
 * A English-Spanish dictionary.
 * 
 * @author Franz Tost
 *
 */
@Component
public class SpanishDictionary implements Dictionary {
	
	// fields /////
	
	private Map<String, String> words = new HashMap<String, String>();
	
	
	// constructor /////
	
	public SpanishDictionary() {
		
		super();
		
		this.words.put("rain", "lluvia");
		this.words.put("wind", "viento");
		this.words.put("sun",  "sol");
		this.words.put("snow", "nieve");
		
	}

	
	// methods /////

	@Override public String translate(final String word) {
		
		return this.words.get(word);
		
	}

}
