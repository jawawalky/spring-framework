/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2018 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex39.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import demo.spring.ex39.dao.ContactDAO;
import demo.spring.ex39.model.Contact;

/**
 * A JPA-based DAO implementation.
 * 
 * @author Franz Tost
 *
 */
@Repository
public class ContactJpaDAO implements ContactDAO {

	// fields /////

	@PersistenceContext
	private EntityManager entityManager;

	
	// methods /////
	
	@Override public Long create(final Contact contact) {
		
		this.entityManager.persist(contact);
		return contact.getId();
		
	}

	@Override public Contact read(final Long id) {
		
		return this.entityManager.find(Contact.class, id);
		
	}

	@Override public void update(final Contact contact) {
		
		this.entityManager.merge(contact);
		
	}

	@Override public void delete(final Long id) {
		
		this.entityManager.remove(this.read(id));
		
	}

	@Override public List<Contact> getAll() {
		
		return
			this.entityManager
				.createQuery("select c from Contact c", Contact.class)
				.getResultList();
		
	}
	
}
