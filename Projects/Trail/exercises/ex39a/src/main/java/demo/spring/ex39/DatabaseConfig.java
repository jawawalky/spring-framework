/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2018 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex39;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * The database configuration.
 * 
 * @author Franz Tost
 *
 */
@Configuration
@ComponentScan({ "demo.spring.ex39.jpa", "demo.spring.ex39.dao" })
@EnableTransactionManagement
public class DatabaseConfig {
	
	// beans /////
	
	@Bean
	public DataSource dataSource() {
		
		try {
			
			Class.forName("org.hsqldb.jdbcDriver");
			return new DriverManagerDataSource("jdbc:hsqldb:mem:demo", "sa", null);
			
		} // try
		catch (ClassNotFoundException e) {
			
			throw new RuntimeException(e);
			
		} // catch
		
	}
	
	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean() {
		
		final LocalContainerEntityManagerFactoryBean factory =
			new LocalContainerEntityManagerFactoryBean();
		factory.setPersistenceUnitName("demo");
		factory.setDataSource(this.dataSource());
		return factory;
		
	}

	@Bean
	public JpaTransactionManager transactionManager() {
		
		return new JpaTransactionManager(this.entityManagerFactoryBean().getObject());
		
	}

}
