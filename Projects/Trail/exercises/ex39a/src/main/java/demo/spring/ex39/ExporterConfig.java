/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2018 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex39;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import demo.spring.ex39.service.AddressBook;

/**
 * The exporter configuration.
 * 
 * @author Franz Tost
 *
 */
@Configuration
public class ExporterConfig {
	
	// fields /////
	
	@Autowired
	private AddressBook addressBook;
	
	
	// beans /////
	
	// TODO
	//
	//  o Create a RMI exporter bean for the address book service.
	//

}
