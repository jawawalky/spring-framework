/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2018 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex40;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import demo.util.Demo;

/**
 * An example for a HTTP exporter.
 * 
 * @author Franz Tost
 *
 */
public class Server {
	
	// constructors /////
	
	private Server() { }

	
	// methods /////

	private void run() {

		try (
			ClassPathXmlApplicationContext context =
			    new ClassPathXmlApplicationContext("beans.xml");
		) {
			
			Demo.log("Server is running ...");
			Demo.input("Press any key to shut down server");

		} // try
			
	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {
		
		new Server().run();
		
	}

}
