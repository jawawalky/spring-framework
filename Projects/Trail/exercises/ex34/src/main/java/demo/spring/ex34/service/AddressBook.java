/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2018 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex34.service;

import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;
import java.util.function.Predicate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;

import demo.spring.ex34.dao.ContactDAO;
import demo.spring.ex34.model.Contact;

/**
 * An address book, which is a service for the administration of
 * {@code Contact}s.
 * 
 * @author Franz Tost
 *
 */
public class AddressBook {

	// fields /////
	
	private AtomicLong idGenerator = new AtomicLong(0L);

	@Autowired
	private PlatformTransactionManager transactionManager;
	
	@Autowired
	private ContactDAO contactDAO;
	
	
	// methods /////
	
	public Long newContact(final Contact contact) {
		
		// TODO
		//
		//  o Begin a transaction, using the method 'beginTransaction(...)'.
		//
		//  o Let the propagation behavior be
		//    'TransactionDefinition.PROPAGATION_REQUIRED'.
		//
		
		try {
			
			Long id = this.idGenerator.addAndGet(1L);
			contact.setId(id);
			this.contactDAO.create(contact);
			
			// TODO
			//
			//  o Commit the transaction.
			//
			
			return id;
			
		} // try
		catch (RuntimeException e) {
			
			// TODO
			//
			//  o Rollback the transaction.
			//
			
			throw e;
			
		} // catch
		
		
	}
	
	public Contact getContact(final Long id) {
		
		// TODO
		//
		//  o Begin a transaction, using the method 'beginTransaction(...)'.
		//
		//  o Let the propagation behavior be
		//    'TransactionDefinition.PROPAGATION_REQUIRED'.
		//
		
		try {
				
			Contact contact = this.contactDAO.read(id);
			
			// TODO
			//
			//  o Commit the transaction.
			//
			
			return contact;
			
		} // try
		catch (RuntimeException e) {
			
			// TODO
			//
			//  o Rollback the transaction.
			//
			
			throw e;
			
		} // catch
		
	}
	
	public void updateContact(final Contact contact) {
		
		// TODO
		//
		//  o Begin a transaction, using the method 'beginTransaction(...)'.
		//
		//  o Let the propagation behavior be
		//    'TransactionDefinition.PROPAGATION_REQUIRED'.
		//
		
		try {
				
			this.contactDAO.update(contact);
			
			// TODO
			//
			//  o Commit the transaction.
			//
			
		} // try
		catch (RuntimeException e) {
			
			// TODO
			//
			//  o Rollback the transaction.
			//
			
			throw e;
			
		} // catch
		
	}
	
	public void removeContact(final Long id) {
		
		// TODO
		//
		//  o Begin a transaction, using the method 'beginTransaction(...)'.
		//
		//  o Let the propagation behavior be
		//    'TransactionDefinition.PROPAGATION_REQUIRED'.
		//
		
		try {
				
			this.contactDAO.delete(id);
			
			// TODO
			//
			//  o Commit the transaction.
			//
			
		} // try
		catch (RuntimeException e) {
			
			// TODO
			//
			//  o Rollback the transaction.
			//
			
			throw e;
			
		} // catch
		
	}
	
	
	public void patchContacts(
		final Predicate<? super Contact> predicate,
		final Consumer<Contact>          patch
	) {
		
		// TODO
		//
		//  o Begin a transaction, using the method 'beginTransaction(...)'.
		//
		//  o Let the propagation behavior be
		//    'TransactionDefinition.PROPAGATION_REQUIRES_NEW'.
		//
		
		try {
			
			this.contactDAO.getAll().stream().filter(predicate).forEach(
					
				contact -> {
					
					patch.accept(contact);
					this.contactDAO.update(contact);
					
				}
				
			);
			
			// TODO
			//
			//  o Commit the transaction.
			//
			
		} // try
		catch (RuntimeException e) {
			
			// TODO
			//
			//  o Rollback the transaction.
			//
			
			throw e;
			
		} // catch
				
	}
	
	public void forEachContact(final Consumer<? super Contact> action) {
		
		this.contactDAO.getAll().forEach(action);
		
	}
	
	private TransactionStatus beginTransaction(final int propagation) {
		
		// TODO
		//
		//  o Create a 'DefaultTransactionDefinition' instance.
		//
		//  o Set its propagation behavior to the value passed to the method.
		//
		//  o Use the transaction manager to create a transaction with
		//    the defined transaction definition.
		//
		
		return null;
		
	}

}
