/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2021 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex50;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * A servlet, which returns <i>Hello</i> as a response to the browser.
 * 
 * @author Franz Tost
 *
 */
public class HelloServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	// methods /////
	
	@Override
	public void doGet(
		final HttpServletRequest request,
		final HttpServletResponse response
	) throws IOException, ServletException {
		
		response.setContentType("text/plain");
		
		// TODO
		//
		//  o Get the 'WebApplicationContext'.
		//
		//    Hint: Use 'ContextLoader'.
		//
		//  o Get the 'Greeter' from the application context'.
		//
		//  o Stream a response with the greetings of the greeter.
		//
		
	}
	
}
