JMX
===


A) Accessing a Management Bean
==============================

  1) Make sure the Server application
     (Project 'Spring [Trail] (43a), JMX Server Exercise') is running.
  
  2) Start the program 'jconsole', which is shipped with the JDK.
  
  3) Connect to the Virtual Machine of the server application.
  
  4) Choose the tab 'MBeans' and select the 'addressBook' MBean.
  
  5) Run the client application.
  
  6) Observe the output.
  
  7) Now change the MBean property 'tuUppercase' to 'true'.
  
  8) Run the client application.
  
  9) Observe the output. The values stored in the database should be
     upper-case values.
     
  10) Now change the MBean property 'tuUppercase' to 'false'.
  
  11) Run the client application.
  
  12) Observe the output. The values stored in the database should be
      lower-case values.
