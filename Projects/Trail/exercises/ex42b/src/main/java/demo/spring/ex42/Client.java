/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2018 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex42;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import demo.spring.ex42.model.AddressBook;
import demo.spring.ex42.model.Contact;
import demo.util.Demo;

/**
 * An example for using DAO objects.
 * 
 * @author Franz Tost
 *
 */
@Configuration
@Component
public class Client {

	// fields
	// ........................................................................
	
	// constructors
	// ........................................................................

	private Client() { }

	// methods
	// ........................................................................

	private void runDemo() {

		Demo.log("Running demo ...");
		
		try (
			ClassPathXmlApplicationContext context =
			    new ClassPathXmlApplicationContext("beans.xml");
		) {
			
			final AddressBook addressBook = context.getBean(AddressBook.class);
	
			Contact contact =
				new Contact("Prime Minister", "Downing Street", 11, "SW1A 2AA", "London");
				
			Demo.log("Creating contact ...");
			final Long id = addressBook.newContact(contact);
			
			Demo.log("Reading contact ...");
			contact = addressBook.getContact(id);
			Demo.log("Contact: %s", contact.toString());
			
			Demo.log("Updating contact ...");
			contact.setHouseNo(10);
			addressBook.updateContact(contact);

			Demo.log("Reading contact ...");
			contact = addressBook.getContact(id);
			Demo.log("Contact: %s", contact.toString());

			Demo.log("Deleting contact ...");
			addressBook.removeContact(id);
			
			Demo.log("Reading contact ...");
			contact = addressBook.getContact(id);
			Demo.log("Contact deleted: %s", (contact == null));
			
		} // try
			
		Demo.log("Finished.");

	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {
		
		try (
			AnnotationConfigApplicationContext context =
			    new AnnotationConfigApplicationContext(Client.class);
		) {
			
			context.getBean(Client.class).runDemo();
	
		} // try
		
	}

}
