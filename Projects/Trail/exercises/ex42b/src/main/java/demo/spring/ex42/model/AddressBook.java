/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2018 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex42.model;

/**
 * A service interface for an address book.
 * 
 * @author Franz Tost
 * 
 * TODO
 * 
 *  o Annotate the interface as a WebService.
 *  
 *  o Set the target namespace to 'http://ws.ex42.spring.demo/'.
 */
public interface AddressBook {

	// methods
	// ........................................................................
	
	// TODO
	//
	//  o Annotate all methods as Web methods.
	//
	
	Long    newContact(Contact contact);
	Contact getContact(Long id);
	void    updateContact(Contact contact);
	void    removeContact(Long id);
	
}
