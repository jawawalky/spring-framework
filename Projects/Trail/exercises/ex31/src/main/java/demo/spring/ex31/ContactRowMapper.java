/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex31;

/**
 * A row mapper maps the values of a result set row to a JavaBean.
 * 
 * @author Franz Tost
 * 
 * TODO
 * 
 *   o Let the class implement the interface 'RowMapper'.
 */
public class ContactRowMapper {

	// methods
	// ........................................................................
	
	// TODO
	//
	//  o Implement the interface method 'mapRow(ResultSet, int)', which should
	//    map a result set record/row to a 'Contact' object.
	//

}
