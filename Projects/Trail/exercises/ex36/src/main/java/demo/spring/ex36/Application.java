/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2018 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex36;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

import demo.spring.ex36.model.Contact;
import demo.spring.ex36.service.AddressBook;
import demo.util.Demo;

/**
 * An example for a transaction proxy.
 * 
 * @author Franz Tost
 *
 */
public class Application {

	// fields /////

	private JdbcTemplate jdbcTemplate;

	private AddressBook addressBook;
	
	
	// constructors /////
	
	private Application() {
		
		super();
		
		try (
//			ClassPathXmlApplicationContext context =
//			    new ClassPathXmlApplicationContext("beans.xml");
			AnnotationConfigApplicationContext context =
				new AnnotationConfigApplicationContext(ApplicationConfiguration.class);
		) {
			
			this.jdbcTemplate = context.getBean(JdbcTemplate.class);
			this.addressBook  = (AddressBook) context.getBean("addressBook");
	
		} // try
		
	}

		
	// methods /////

	private void run() {

		Demo.log("Running demo ...");
		
		this.setupSchema();
		
		Demo.log("Creating contacts ...");
		this.createContacts();
		this.showContacts();
		
		Demo.log("Patchting contacts ...");
		this.patchContacts();
		this.showContacts();
		
		Demo.log("Finished.");

	}
	
	private void createContacts() {
		
		this.addressBook.newContact(new Contact("Hofbr\u00e4uhaus",        "Platzl",         9, "80331", "Hamburg"));
		this.addressBook.newContact(new Contact("Cl\u00e4rchens Ballhaus", "Auguststraße",  24, "10117", "Berlin"));
		this.addressBook.newContact(new Contact("Lenbachhaus",             "Luisenstrasse", 33, "80333", "Hamburg"));
		
	}
	
	private void patchContacts() {
		
		this.addressBook.patchContacts(
			contact -> "Hamburg".equals(contact.getCity()),
			contact -> contact.setCity("M\u00fcnchen")
		);
		
	}
	
	private void showContacts() {
		
		this.addressBook.forEachContact(
			contact -> Demo.log(contact.toString())
		);
		
	}
	
    private void setupSchema() {
		
        Demo.log("Creating schema ...");
		
        this.jdbcTemplate.update(
            "CREATE TABLE Addressbook (" +
            "id INTEGER, "               +
            "name VARCHAR(50), "         +
            "street VARCHAR(100), "      +
            "houseNo INTEGER, "          +
            "zipCode VARCHAR(10), "      +
            "city VARCHAR(30))"
        );
	    
	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {
		
		new Application().run();
		
	}

}
