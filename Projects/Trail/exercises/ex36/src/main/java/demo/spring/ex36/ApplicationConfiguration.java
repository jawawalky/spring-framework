/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2018 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex36;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.transaction.PlatformTransactionManager;

import demo.spring.ex36.dao.ContactDAO;
import demo.spring.ex36.dao.DaoAddressBook;
import demo.spring.ex36.jdbc.ContactJdbcDAO;
import demo.spring.ex36.service.AddressBook;

/**
 * An example for a transaction proxy.
 * 
 * @author Franz Tost
 *
 */
@Configuration
public class ApplicationConfiguration {

	// beans /////
	
	@Bean
	public DataSource dataSource() {
		
		try {
			
			Class.forName("org.hsqldb.jdbcDriver");
			return new DriverManagerDataSource("jdbc:hsqldb:mem:demo", "sa", null);
			
		} // try
		catch (ClassNotFoundException e) {
			
			throw new RuntimeException(e);
			
		} // catch
		
	}
	
	@Bean
	public JdbcTemplate jdbcTemplate() {
		
		return new JdbcTemplate(this.dataSource());
		
	}
	
	@Bean
	public PlatformTransactionManager transactionManager() {
		
		return new DataSourceTransactionManager(this.dataSource());
		
	}
	
	@Bean
	public ContactDAO contactDAO() {
		
		return new ContactJdbcDAO(this.dataSource());
		
	}
	
	@Bean
	public AddressBook daoAddressBook() {
		
		return new DaoAddressBook(this.contactDAO());
		
	}
	
	// TODO
	//
	//    o Define a bean with the ID 'addressBook'.
	//
	//    o It should use
	//      'org.springframework.transaction.interceptor.TransactionProxyFactoryBean'
	//      for the bean creation.
	//
	//    o Set the following properties of the bean
	//
	//        o 'proxyTargetClass'   (false)
	//        o 'target'             (the address book DAO)
	//        o 'transactionManager' (the transaction manager)
	//        o 'transactionAttributeSource'
	//          (a bean of 'org.springframework.transaction.interceptor.MatchAlwaysTransactionAttributeSource')
	//
	  
}
