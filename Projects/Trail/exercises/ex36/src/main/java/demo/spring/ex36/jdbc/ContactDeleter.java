/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2018 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex36.jdbc;

import java.sql.Types;

import javax.sql.DataSource;

import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

/**
 * A SQL object, which deletes a contact.
 * 
 * @author Franz Tost
 */
public class ContactDeleter extends SqlUpdate {

	// constructors /////

	public ContactDeleter(final DataSource dataSource) {

		super(
			dataSource,
			"DELETE FROM Addressbook WHERE id = ?"
		);
		
		this.declareParameter(new SqlParameter("id", Types.INTEGER));

	}

	
	// methods /////

	public int delete(final Long id) { return this.update(id); }

}
