/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2018 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex42.ws;

import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import demo.spring.ex42.model.AddressBook;

/**
 * A DAO-based address book implementation.
 * 
 * @author Franz Tost
 * 
 * TODO
 * 
 *  o Annotate this class as a WebService.
 *  
 *  o Give it the service name 'AddressBookEndpoint'.
 *  
 *  o Let it implement the 'AddressBook' interface.
 */
public class AddressBookEndpoint
	extends
		SpringBeanAutowiringSupport
{

	// fields
	// ........................................................................

	private AddressBook addressBook;
	
	// constructors
	// ........................................................................
	
	public AddressBookEndpoint(final AddressBook addressBook) {
		
		super();
		
		this.addressBook = addressBook;
		
	}
	
	// methods
	// ........................................................................
	
	// TODO
	//
	//  o Implement the 'AddressBook' interface methods.
	//
	//  o Delegate the processing in each method to the actual address book.
	//
	//  o Annotate each method as a Web method.
	//
	
}
