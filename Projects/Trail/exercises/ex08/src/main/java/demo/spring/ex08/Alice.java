/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex08;

/**
 * The player <i>Alice</i>.
 * 
 * @author Franz Tost
 *
 */
public class Alice {
	
	// fields /////
	
	// TODO
	//
	//  o Add a global counter, which counts all games that have been
	//    played.
	//
	//  o Add an individual counter, which counts all games won by Alice.
	//
	
	
	// methods /////
	
	public void wins() {

		// TODO
		//
		//  o Increase the game count and the win count.
		//
		
	}
	
	public void show() {

		// TODO
		//
		//  o Display on the console, how many games have been played and
		//    how many have been won by Alice.
		//
		
	}

}
