/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex08;

/**
 * An individual counter.
 * 
 * @author Franz Tost
 *
 */
public class GlobalCounter {
	
	// TODO
	//
	//  o Create a global counter component.
	//
	//  o The counter must be able to increase the internal count and
	//    it must be able to return the current count.
	//
	
	
	// fields /////
	
	
	// methods /////
	
}
