/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex08;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import demo.util.Demo;

/**
 * In this demo you learn, how to autowire beans.
 * 
 * @author Franz Tost
 *
 */
@Component
public class Application {
	
	// fields /////

	
	// constructors /////

	private Application() { }
	

	// methods /////

	private void runDemo() {

		Demo.log("Running application ...");
		
		// TODO
		//
		//  o Let Duke and Alice play some games.
		//
		//    Hint: Inject Duke and Alice into the application.
		//
		//  o Let Duke win some games and also Alice.
		//
		//  o Check, if the counters count correctly.
		//		
		
		Demo.log("Finished.");

	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		try (
			AnnotationConfigApplicationContext context =
			    new AnnotationConfigApplicationContext(
			    	ApplicationConfiguration.class
			    );
		) {
			
			final Application application = context.getBean(Application.class);
			application.runDemo();
	
		} // try
			
	}

}
