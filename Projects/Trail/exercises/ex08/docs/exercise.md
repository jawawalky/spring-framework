# Bean Scopes

## Your Exercise

In this exercise two players
([Duke](../src/main/java/demo/spring/ex08/Duke.java) and
[Alice](../src/main/java/demo/spring/ex08/Alice.java)) should play some games
against each other. Sometimes Duke wins and sometimes Alice wins. With a global
counter we want to count the number of games played. With individual counters
we would like to count the games won by each player.

The two counter component classes will have an identical implementation.
In what do you think they will differ?

In order to solve this problem, you will have to work on the following classes

- [Counter](../src/main/java/demo/spring/ex08/Counter.java)
- [GlobalCounter](../src/main/java/demo/spring/ex08/GlobalCounter.java)
- [Duke](../src/main/java/demo/spring/ex08/Duke.java)
- [Alice](../src/main/java/demo/spring/ex08/Alice.java)
- [Application](../src/main/java/demo/spring/ex08/Application.java)

**Hint:**

- The implementations of `Counter` and `GlobalCounter` will be the same.
  (Copy and Paste ;-)
- The implementations of `Duke` and `Alice` will be nearly the same.
  (Copy and Paste ;-)
