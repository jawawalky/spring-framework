# A Bean's Life-cycle

## Caching Slow Actions

Our translations have become a bit slow (open
[GermanDictionary.java](../src/main/java/demo/spring/ex07/GermanDictionary.java))
and observe the `translate(String)` method. We translate four words in our
[application](../src/main/java/demo/spring/ex07/Main.java)), which takes about
400 milliseconds. We can speed that up, if our Translator caches the words
that are most often translated.

Statistics give rise to the assumption that the words *rain* and *sun* are
the words, which are translated most frequently. If we can create a cache
in our [Translator](../src/main/java/demo/spring/ex07/Translator.java) bean,
then we should be able to speed up translations by 200 ms.

## Your Exercise

1. Equip the [Translator](../src/main/java/demo/spring/ex07/Translator.java)
   with some kind of caching mechanism.
1. Cache the translations for *rain* and *sun* at the startup of
   the translator.
1. Destroy the cache, when the translator is destroyed.
1. Observe the performance boost. ;-)

**Hints:**

- Use a simple `HashMap` for your cache.
- Use life-cycle methods for the initialization and destruction of the cache.
