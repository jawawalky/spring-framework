/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2018 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex38.jpa;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

import demo.spring.ex38.dao.ContactDAO;
import demo.spring.ex38.model.Contact;

/**
 * A JPA-based DAO implementation.
 * 
 * @author Franz Tost
 *
 */
@Repository
public class ContactJpaDAO implements ContactDAO {

	// fields /////

	// TODO
	//
	//  o Inject an entity manager.
	//
	//    Hint: Use the 'javax.persistence.PersistenceContext' annotation for
	//          injection.
	//
	
	private EntityManager entityManager;

	
	// methods /////
	
	@Override public Long create(final Contact contact) {
		
		// TODO
		//
		//  o Use the entity manager to persist the contact.
		//
		//  o Return the ID of the persisted entity.
		//
		
		return null;
		
	}

	@Override public Contact read(final Long id) {
		
		// TODO
		//
		//  o Use the entity manager to find the contact with the specified ID.
		//
		//  o Return the contact.
		//
		
		return null;
		
	}

	@Override public void update(final Contact contact) {
		
		// TODO
		//
		//  o Use the entity manager to merge the updated contact with
		//    the persisted contact.
		//
		
	}

	@Override public void delete(final Long id) {
		
		// TODO
		//
		//  o Use the entity manager to remove the contact with
		//    the specified ID.
		//
		
	}

	@Override public List<Contact> getAll() {
		
		// TODO
		//
		//  o Use the entity manager to query all contacts.
		//
		//    Hint: Use the following HQL command
		//
		//          'select c from Contact c'
		//
		
		
		return null;
		
	}
	
}
