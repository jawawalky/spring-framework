/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2018 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex38.model;

import java.io.Serializable;

/**
 * A JavaBean modeling a contact.
 * 
 * @author Franz Tost
 *
 * TODO
 * 
 *   o Make this class an entity class.
 *   
 *     Hint: Use the 'javax.persistence.Entity' annotation.
 *     
 *   o Map contact entities to the table 'AddressBook'.
 *   
 *     Hint: Use the 'javax.persistence.Table' annotation.
 */
public class Contact implements Serializable {

	private static final long serialVersionUID = 1L;

	// fields /////

	// TODO
	//
	//  o Apply the following annotations, where necessary
	//
	//      o 'javax.persistence.Id'
	//      o 'javax.persistence.GeneratedValue'
	//      o 'javax.persistence.Basic'
	//
	
	private Long   id;

	private String name;

	private String street;

	private int    houseNo;

	private String zipCode;

	private String city;

	
	// constructors /////

	public Contact() {

		super();

	}

	public Contact(
		final String name,
		final String street,
		final int    houseNo,
		final String zipCode,
		final String city
	) {

		super();

		this.name    = name;
		this.street  = street;
		this.houseNo = houseNo;
		this.zipCode = zipCode;
		this.city    = city;

	}

	public Contact(
		final Long   id,
		final String name,
		final String street,
		final int    houseNo,
		final String zipCode,
		final String city
	) {

		super();

		this.id      = id;
		this.name    = name;
		this.street  = street;
		this.houseNo = houseNo;
		this.zipCode = zipCode;
		this.city    = city;

	}

	// methods /////

	public Long   getId()                          { return this.id;         }
	public void   setId(final Long id)             { this.id = id;           }

	public String getName()                        { return this.name;       }
	public void   setName(final String name)       { this.name = name;       }

	public String getStreet()                      { return this.street;     }
	public void   setStreet(final String street)   { this.street = street;   }

	public int    getHouseNo()                     { return this.houseNo;    }
	public void   setHouseNo(final int houseNo)    { this.houseNo = houseNo; }

	public String getZipCode()                     { return this.zipCode;    }
	public void   setZipCode(final String zipCode) { this.zipCode = zipCode; }

	public String getCity()                        { return this.city;       }
	public void   setCity(final String city)       { this.city = city;       }

	public String toString() {
		
		return
			"ID = "       + this.getId()      + ", " +
			"Name = "     + this.getName()    + ", " +
			"Street = "   + this.getStreet()  + ", " +
			"House No = " + this.getHouseNo() + ", " +
			"ZIP Code = " + this.getZipCode() + ", " +
			"City = "     + this.getCity();
		
	}

}
