/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2018 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex38.dao;

import java.util.function.Consumer;
import java.util.function.Predicate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import demo.spring.ex38.model.Contact;
import demo.spring.ex38.service.AddressBook;

/**
 * A DAO-based address book implementation.
 * 
 * @author Franz Tost
 *
 */
@Transactional @Service
public class DaoAddressBook implements AddressBook {

	// fields /////

	@Autowired
	private ContactDAO contactDAO;
	
	
	// methods /////
	
	@Override public Long newContact(final Contact contact) {
		
		return this.contactDAO.create(contact);
		
	}
	
	@Override public Contact getContact(final Long id) {
		
		return contactDAO.read(id);
		
	}
	
	@Override public void updateContact(final Contact contact) {
		
		this.contactDAO.update(contact);
		
	}
	
	@Override public void removeContact(final Long id) {
		
		this.contactDAO.delete(id);
		
	}
	
	
	@Override public void patchContacts(
		final Predicate<? super Contact> predicate,
		final Consumer<Contact>          patch
	) {

		this.contactDAO.getAll().stream().filter(predicate).forEach(
			contact -> {
				patch.accept(contact);
				this.contactDAO.update(contact);
			}
		);
		
	}
	
	@Override public void forEachContact(
		final Consumer<? super Contact> action
	) {
		
		this.contactDAO.getAll().forEach(action);
		
	}
	
}
