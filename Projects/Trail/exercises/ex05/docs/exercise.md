# Autowiring Beans

## Defining Components

1. Look at the available classes and decide, which of them should be
   component classes.
1. Annotate those classes with `@Component`.

## Autowiring Bean

1. The [translator](../src/main/java/demo/spring/ex05/Translator.java)
   should use a [dictionary](../src/main/java/demo/spring/ex05/Dictionary.java)
   for translations.
1. Add required field and autowire then according beans.

## Running Application

1. Open the [application](../src/main/java/demo/spring/ex05/Main.java).
1. Get a translator instance and translate some words.
   (See the instructions of the *TODO* section.)
   