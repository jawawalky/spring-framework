/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex05;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import demo.util.Demo;

/**
 * In this demo you learn, how to autowire beans.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors /////

	private Main() { }
	

	// methods /////

	private void runDemo() {

		Demo.log("Running demo ...");
		
		try (
			AnnotationConfigApplicationContext context =
			    new AnnotationConfigApplicationContext(
			    	ApplicationConfiguration.class
			    );
		) {
			
			// TODO
			//
			//  o Get a 'Translator' instance.
			//
			//  o Translate the words
			//
			//      o 'rain'
			//      o 'sun'
			//      o 'snow'
			//      o 'wind'
			//
	
		} // try
		
		Demo.log("Finished.");

	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Main().runDemo();

	}

}
