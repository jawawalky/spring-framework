/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex05;

/**
 * A translator object, which autowires a {@link Dictionary} for
 * the translations.
 * 
 * @author Franz Tost
 *
 */
public class Translator {
	
	// fields /////
	
	// TODO
	//
	//  o Inject (Autowire) a 'Dictionary' into the translator.
	//
	
	
	// methods /////
	
	public void translate(final String word) {
		
		// TODO
		//
		//  o Use the autowired dictionary to translate the word.
		//
		//  o Print the translation on the console.
		//
		
	}

}
