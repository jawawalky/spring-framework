/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2018 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex43.management;

import org.springframework.stereotype.Component;

/**
 * The management bean for the address book.
 * 
 * @author Franz Tost
 *
 */
@Component("addressBookConfig")
public class AddressBookConfig {

	// fields /////
	
	private boolean toUpperCase;

	
	// methods /////
	
	public boolean isToUpperCase()                           { return this.toUpperCase;        }
	public void    setToUpperCase(final boolean toUpperCase) { this.toUpperCase = toUpperCase; }
		
}
