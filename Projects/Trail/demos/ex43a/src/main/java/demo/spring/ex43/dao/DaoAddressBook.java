/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2018 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex43.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import demo.spring.ex43.management.AddressBookConfig;
import demo.spring.ex43.model.Contact;
import demo.spring.ex43.service.AddressBook;

/**
 * A DAO-based address book implementation.
 * 
 * @author Franz Tost
 *
 */
@Transactional @Service("AddressBookService")
public class DaoAddressBook implements AddressBook {

	// fields /////

	@Autowired
	private ContactDAO contactDAO;
	
	@Autowired
	private AddressBookConfig config;
	
	
	// methods /////
	
	@Override public Long newContact(final Contact contact) {
		
		if (this.config.isToUpperCase()) {
			
			contact.setName(contact.getName().toUpperCase());
			contact.setStreet(contact.getStreet().toUpperCase());
			contact.setCity(contact.getCity().toUpperCase());
			
		} // if
		
		return this.contactDAO.create(contact);
		
	}
	
	@Override public Contact getContact(final Long id) {
		
		return contactDAO.read(id);
		
	}
	
	@Override public void updateContact(final Contact contact) {
		
		this.contactDAO.update(contact);
		
	}
	
	@Override public void removeContact(final Long id) {
		
		this.contactDAO.delete(id);
		
	}
	
	
}
