/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex02;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * A Java class containing the bean definitions for airports in Germany.
 * 
 * @author Franz Tost
 *
 */
@Configuration          // <- Marks this class as a configuration class.
public class Germany {  //    The 'AnnotationConfigApplicationContext' gets
                        //    its bean definitions from configuration classes.

	// methods /////

	@Bean                    // <- Binds the object returned by this method
	public Airport fra() {   //    in the application context.
		
		return new Airport(
			"FRA",
			"Frankfurt International Airport",
			"Frankfurt am Main"
		);

	}

}
