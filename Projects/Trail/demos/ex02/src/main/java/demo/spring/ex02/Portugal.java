/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex02;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * A Java class containing the bean definitions for airports in Portugal.
 * 
 * @author Franz Tost
 *
 */
@Configuration
public class Portugal {

	// methods /////

	@Bean
	public Airport lis() {
		
		return new Airport(
			"LIS",
			"Humberto Delgado",
			"Lisboa"
		);

	}

}
