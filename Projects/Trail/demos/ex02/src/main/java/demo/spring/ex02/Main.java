/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex02;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import demo.util.Demo;

/**
 * This is a simple demo, which shows you how you can configure your
 * beans with Java-based configuration classes.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors /////

	private Main() { }

	
	// methods /////

	private void runDemo() {

		Demo.log("Running demo ...");
		
		try (
			AnnotationConfigApplicationContext context =
			    new AnnotationConfigApplicationContext(
			    	Germany.class,
			    	Spain.class,
			    	Portugal.class
			    );
		) {
	
			Airport bcn = (Airport) context.getBean("bcn");
			Airport fra = (Airport) context.getBean("fra");
			Airport lis = (Airport) context.getBean("lis");
			Airport mad = (Airport) context.getBean("mad");
			
			Demo.log(bcn.toString());
			Demo.log(fra.toString());
			Demo.log(lis.toString());
			Demo.log(mad.toString());
		
		} // try
		
		Demo.log("Finished.");

	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Main().runDemo();

	}

}
