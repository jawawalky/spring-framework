/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2018 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex39;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.rmi.RmiProxyFactoryBean;

import demo.spring.ex39.service.AddressBook;

/**
 * Java configuration of the client application.
 * 
 * @author Franz Tost
 *
 */
@SuppressWarnings("deprecation")
@Configuration
public class ClientConfig {
	
	@Bean
	public RmiProxyFactoryBean addressBook() {
		
		final RmiProxyFactoryBean factory = new RmiProxyFactoryBean();
		factory.setServiceUrl("rmi://localhost:1199/AddressBook");
		factory.setServiceInterface(AddressBook.class);
		return factory;
		
	}
	
}
