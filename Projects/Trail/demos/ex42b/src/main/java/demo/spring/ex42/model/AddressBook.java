/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2018 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex42.model;

import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 * A service interface for an address book.
 * 
 * @author Franz Tost
 *
 */
@WebService(targetNamespace = "http://ws.ex42.spring.demo/")
public interface AddressBook {

	// methods
	// ........................................................................
	
	@WebMethod Long    newContact(Contact contact);
	@WebMethod Contact getContact(Long id);
	@WebMethod void    updateContact(Contact contact);
	@WebMethod void    removeContact(Long id);
	
}
