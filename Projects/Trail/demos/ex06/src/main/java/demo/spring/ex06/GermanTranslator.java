/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex06;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import demo.util.Demo;

/**
 * An English-German translator.
 * 
 * @author Franz Tost
 *
 */
@Component
public class GermanTranslator {
	
	// fields /////
	
	@Autowired @German
	private Dictionary dictionary;

	
	// methods /////
	
	public void translate(final String word) {
		
		Demo.log(word + " --> " + this.dictionary.translate(word));
		
	}

}
