# Qualifiers

## The Problem

If we use a type to identify a bean, but more than one bean has been registered
for the same type, then this is ambiguous. The context cannot decide, which
bean object it should return. We can help *Spring* with the decision by adding
a further criteria, which makes the ambiguous beans uniquely identifiable.
The additional criteria is called a qualifier.

## How to Define a Qualifier

We can create a qualifier by defining a new annotation, which is annotated
with `@Qualifier`.

```
@Target({ TYPE, FIELD })
@Retention(RUNTIME)
@Qualifier
public @interface Savings { }
```

```
@Target({ TYPE, FIELD })
@Retention(RUNTIME)
@Qualifier
public @interface Checking { }
```

## How to Use a Qualifier

The qualifier annotation is used in two different places. On the component
class, which collides with some other component.

```
@Component @Savings
public class SavingsAccount implements Account { }
```

```
@Component @Checking
public class CheckingAccount implements Account { }
```

> Both `SavingsAccount` and `CheckingAccount` are `Account`s. But thanks
> to the different qualifiers, they can be distinguished from
> each other.

The second place, where the qualifier annotation must be used,
is where a bean gets injected/autowired.

The following code would be ambiguous.

```
@Autowired
public Account account;
```

Both a `SavingsAccount` and a `CheckingAccount` would do. The solution would
be

```
@Autowired @Savings
public Account savingsAccount;
```

if we wanted a `SavingsAccount` and

```
@Autowired @Checking
public Account checkingAccount;
```

if we wanted a `CheckingAccount`.
