/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2018 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex37.jdbc;

import java.sql.Types;

import javax.sql.DataSource;

import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

import demo.spring.ex37.model.Contact;

/**
 * A SQL object, which updates a contact.
 * 
 * @author Franz Tost
 */
public class ContactUpdater extends SqlUpdate {

	// constructors
	// ........................................................................

	public ContactUpdater(final DataSource dataSource) {

		super(
			dataSource,
			"UPDATE Addressbook SET " +
			"name = ?, " +
			"street = ?, " +
			"houseNo = ?, " +
			"zipCode = ?, " +
			"city = ? " +
			"WHERE id = ?"
		);
		
		this.declareParameter(new SqlParameter("id",      Types.INTEGER));
		this.declareParameter(new SqlParameter("name",    Types.VARCHAR));
		this.declareParameter(new SqlParameter("street",  Types.VARCHAR));
		this.declareParameter(new SqlParameter("houseNo", Types.INTEGER));
		this.declareParameter(new SqlParameter("zipCode", Types.VARCHAR));
		this.declareParameter(new SqlParameter("city",    Types.VARCHAR));

	}

	// methods
	// ........................................................................

	public int update(final Contact contact) {

		return this.update(
			contact.getName(),
			contact.getStreet(),
			contact.getHouseNo(),
			contact.getZipCode(),
			contact.getCity(),
			contact.getId()
		);
		
	}

}
