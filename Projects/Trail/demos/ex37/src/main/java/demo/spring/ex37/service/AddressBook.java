/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2018 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex37.service;

import java.util.function.Consumer;
import java.util.function.Predicate;

import demo.spring.ex37.model.Contact;

/**
 * A service interface for an address book.
 * 
 * @author Franz Tost
 *
 */
public interface AddressBook {

	// methods
	// ........................................................................
	
	Long    newContact(Contact contact);
	Contact getContact(Long id);
	void    updateContact(Contact contact);
	void    removeContact(Long id);
	
	void patchContacts(
		Predicate<? super Contact> predicate,
		Consumer<Contact>          patch
	);
	void forEachContact(Consumer<? super Contact> action);
	
}
