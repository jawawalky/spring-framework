/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2018 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex37.dao;

import static org.springframework.transaction.annotation.Propagation.NEVER;
import static org.springframework.transaction.annotation.Propagation.REQUIRED;
import static org.springframework.transaction.annotation.Propagation.REQUIRES_NEW;
import static org.springframework.transaction.annotation.Propagation.SUPPORTS;

import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;
import java.util.function.Predicate;

import org.springframework.transaction.annotation.Transactional;

import demo.spring.ex37.model.Contact;
import demo.spring.ex37.service.AddressBook;

/**
 * A DAO-based address book implementation.
 * 
 * @author Franz Tost
 *
 */
@Transactional(propagation = REQUIRED)
public class DaoAddressBook implements AddressBook {

	// fields
	// ........................................................................
	
	private AtomicLong idGenerator = new AtomicLong(0L);
	
	private ContactDAO contactDAO;
	
	// constructors
	// ........................................................................
	
	public DaoAddressBook(final ContactDAO contactDAO) {
		
		super();
		
		this.contactDAO = contactDAO;
		
	}
	
	// methods
	// ........................................................................
	
	@Override public Long newContact(final Contact contact) {
		
		Long id = idGenerator.addAndGet(1L);
		contact.setId(id);
		this.contactDAO.create(contact);
		return id;
		
	}
	
	@Transactional(propagation = SUPPORTS)
	@Override public Contact getContact(final Long id) {
		
		return contactDAO.read(id);
		
	}
	
	@Override public void updateContact(final Contact contact) {
		
		this.contactDAO.update(contact);
		
	}
	
	@Override public void removeContact(final Long id) {
		
		this.contactDAO.delete(id);
		
	}
	
	
	@Transactional(propagation = REQUIRES_NEW)
	@Override public void patchContacts(
		final Predicate<? super Contact> predicate,
		final Consumer<Contact>          patch
	) {

		this.contactDAO.getAll().stream().filter(predicate).forEach(
			contact -> {
				patch.accept(contact);
				this.contactDAO.update(contact);
			}
		);
		
	}
	
	@Transactional(propagation = NEVER)
	@Override public void forEachContact(
		final Consumer<? super Contact> action
	) {
		
		this.contactDAO.getAll().forEach(action);
		
	}
	
}
