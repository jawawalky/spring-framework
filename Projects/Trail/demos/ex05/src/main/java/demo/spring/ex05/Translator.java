/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex05;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import demo.util.Demo;

/**
 * A translator object, which autowires a {@link Dictionary} for
 * the translations.
 * 
 * @author Franz Tost
 *
 */
@Component
public class Translator {
	
	// fields /////
	
	@Autowired
	private Dictionary dictionary;

	
	// methods /////
	
	public void translate(final String word) {
		
		Demo.log(word + " --> " + this.dictionary.translate(word));
		
	}

}
