# Autowiring Beans

## How to Autowire Beans

Dependencies between objects are expressed by references. References between
beans controlled by the application context can be set automatically by
the context. That is called autowiring.

*Spring* does not set dependencies by itself. We need to tell *Spring*,
which dependencies we want to be resolved automatically. By adding
the annotation `@Autowired` to a field, constructor, method or parameter, we
can tell *Spring*, which dependencies should be resolved.

**Example:**

If a bean of type `Dictionary` is present in the context, then it will be
injected into the field `dictionary`.

```
@Autowired
private Dictionary dictionary;
```
