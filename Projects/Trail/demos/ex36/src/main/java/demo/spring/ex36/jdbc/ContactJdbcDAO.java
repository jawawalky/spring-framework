/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2018 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex36.jdbc;

import java.util.List;

import javax.sql.DataSource;

import demo.spring.ex36.dao.ContactDAO;
import demo.spring.ex36.model.Contact;

/**
 * A DAO interface for {@code Contact}s.
 * 
 * @author Franz Tost
 *
 */
public class ContactJdbcDAO implements ContactDAO {

	// fields /////

	private ContactCreator contactCreator;
	private ContactReader  contactReader;
	private ContactUpdater contactUpdater;
	private ContactDeleter contactDeleter;
	private AllContacts    allContacts;
	
	
	// constructors /////
	
	public ContactJdbcDAO(final DataSource dataSource) {
		
		super();
		
		this.contactCreator = new ContactCreator(dataSource);
		this.contactReader  = new ContactReader(dataSource);
		this.contactUpdater = new ContactUpdater(dataSource);
		this.contactDeleter = new ContactDeleter(dataSource);
		this.allContacts    = new AllContacts(dataSource);
		
	}

	
	// methods /////
	
	@Override public Long create(final Contact contact) {
		
		this.contactCreator.create(contact);
		return contact.getId();
		
	}

	@Override public Contact read(final Long id) {
		
		return this.contactReader.read(id);
		
	}

	@Override public void update(final Contact contact) {
		
		this.contactUpdater.update(contact);
		
	}

	@Override public void delete(final Long id) {
		
		this.contactDeleter.delete(id);
		
	}

	@Override public List<Contact> getAll() {
		
		return this.allContacts.execute();
		
	}

}
