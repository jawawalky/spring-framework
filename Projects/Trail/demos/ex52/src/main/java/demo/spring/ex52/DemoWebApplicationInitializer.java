/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2021 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex52;

import org.springframework.stereotype.Component;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRegistration;

/**
 * An {@code WebApplicationInitializer} for this demo application.
 * 
 * @author Franz Tost
 *
 */
@Component
public class DemoWebApplicationInitializer implements WebApplicationInitializer {
	
	// methods /////
	
	@Override
	public void onStartup(
		final ServletContext servletContext
	) throws ServletException {
		
		final AnnotationConfigWebApplicationContext context =
			new AnnotationConfigWebApplicationContext();
		
		context.scan("demo.spring.ex52");
		context.refresh();
		
		servletContext.addListener(new ContextLoaderListener(context));
		
		final ServletRegistration.Dynamic helloServlet =
			servletContext.addServlet("helloServlet", new HelloServlet());
		helloServlet.setLoadOnStartup(1);
		helloServlet.addMapping("/hello");
	
	}

}
