# XML-based Configuration

## How to Define a Bean

When we use XML-based configurations, then we write XML files containing bean
definitions. Beans are defined by `<bean>` elements and their properties can
be set by embedded `<property>` elements.

**Example:**

```
<bean id="fra" class="demo.spring.ex01.Airport">

	<property name="code" value="FRA"/>
	<property name="name" value="Frankfurt International Airport"/>
	<property name="city" value="Frankfurt am Main"/>
	
</bean>
```

## How to Access a Bean

Beans are bound in the application context. In the beginning of *Spring* that
was the `BeanFactory`. Today we use some sub-class of the `ApplicationContext`.
When the application context is created, it loads the configured beans. The beans
can be accessed by their name with the method `getBean(String)` of 
the application context.

**Example:**

```
try (
	ClassPathXmlApplicationContext context =
	    new ClassPathXmlApplicationContext("beans.xml");
) {

	Airport airport = (Airport) context.getBean("fra");
	...

} // try
```
