# Events

## Publisher-Subscriber

The *Spring* event mechanism is a simple *publisher-subscriber* mechanism
that allows beans to communicate via events.

## How to Define an Event

Event classes must be derived from `ApplicationEvent`. They can transport any
kind of data. One parameter is the event source, which fired the event.

```
public class TranslationEvent extends ApplicationEvent {

  private String word;
  private String translation;
  
  public TranslationEvent(
    Object source,
    String word,
    String translation
  ) {
    super(source);
    this.word = word;
    this.translation = translation;
  }
  
  public String getWord() {
    return this.word;
  }
  
  public String getTranslation() {
    return this.translation;
  }
  
}
```

## Firing an Event

If some bean wants to send an event, then it needs
an `ApplicationEventPublisher`. The event publisher can be injected into any
*Spring* bean.

```
@Component
public class Translator {

  @Autowired
  private Dictionary dictionary;

  @Autowired
  private ApplicationEventPublisher applicationEventPublisher;
  
  public void translate(String word) {
    this.applicationEventPublisher.publishEvent(
      new TranslationEvent(
        this,
        word,
        this.dictionary.translate(word)
      )
    );
  }
  
}
```

## Listening to Events

Beans that should listen to events must implement the `ApplicationListener`
interface. Events can be processed in the `onApplicationEvent(...)` method.

```
@Component
public class TranslationMonitor
  implements ApplicationListener<TranslationEvent> {

  @Override
  public void onApplicationEvent(final TranslationEvent event) {
    System.out.println(
      event.getWord + " --> " + event.getTranslation()
    );
  }
  
}
```
