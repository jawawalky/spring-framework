/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex10;

import java.util.HashMap;
import java.util.Map;

import org.springframework.context.ApplicationListener;

import demo.util.Demo;

/**
 * A bean, which counts the played games and stores its winners.
 * 
 * @author Franz Tost
 *
 */
public class Scores implements ApplicationListener<GameEvent> {
	
	// fields /////
	
	private int gameCount;
	
	private Map<String, Integer> winners = new HashMap<>();
	
	
	// methods /////
	
	public int getGameCount() {
		
		return this.gameCount;
		
	}

	@Override
	public void onApplicationEvent(final GameEvent event) {
		
		this.gameCount++;
		this.winners.compute(
			event.getName(),
			(name, wins) -> (wins != null) ? wins + 1 : 1
		);
		
	}
	
	public void show() {
		
		this.winners
			.entrySet()
			.forEach(e -> Demo.log(
				"%s has won %d out of %d games.",
				e.getKey(),
				e.getValue(),
				this.gameCount
			));
		
	}

}
