/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex10;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;

/**
 * A player, who sends an event, when he or she wins a game.
 * 
 * @author Franz Tost
 *
 */
public class Player {
	
	// fields /////
	
	private String name;
	
	@Autowired
    private ApplicationEventPublisher applicationEventPublisher;
	
	
	// methods /////
	
	public void setName(final String name) {
		
		this.name = name;
		
	}
	
	public void wins() {
		
		this.applicationEventPublisher.publishEvent(
			new GameEvent(this, this.name)
		);
		
	}
	
}
