/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex10;

import org.springframework.context.ApplicationEvent;

/**
 * An event fired, when a game is over.
 * 
 * @author Franz Tost
 *
 */
public class GameEvent extends ApplicationEvent {
	
	private static final long serialVersionUID = 1L;
	

	// fields /////
	
	private String name;
	
	
	// constructors /////
	
	public GameEvent(final Object source, final String name) {
		
		super(source);
		
		this.name = name;
		
	}

	
	// methods /////
	
	public String getName() {
		
		return this.name;
		
	}
	
}
