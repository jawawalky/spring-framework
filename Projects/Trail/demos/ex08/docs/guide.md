# Bean Scopes

## What is Bean Scope

The bean scope defines, when a new instance of some bean is created and when
it will be destroyed. Two basic scopes known by *Spring* are *singleton* and
*prototype*. In Web applications there are further scopes, such as *request*,
*session* and *application*.

The default scope is *singleton*. Only one instance of that bean type will be
created. Each time we inject it, the same instance will be provided.

In case of a *prototype* bean, each time we inject it, a new instance will
be created. So every *client* gets its own bean, which is not shared by
the other clients.

The scope can be defined by the `@Scope` annotation on the bean class.

**Example:**

```
@Component
@Scope("prototype")
public class Counter { ... }
```
