/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex08;

import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * An individual counter.
 * 
 * @author Franz Tost
 *
 */
@Component
@Scope(SCOPE_PROTOTYPE)
public class Counter {
	
	// fields /////
	
	private int count;
	
	
	// methods /////
	
	public void increase() {
		
		this.count++;
		
	}
	
	public int getCount() {
		
		return this.count;
		
	}

}
