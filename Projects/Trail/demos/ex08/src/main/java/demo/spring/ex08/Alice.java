/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex08;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import demo.util.Demo;

/**
 * The player <i>Alice</i>.
 * 
 * @author Franz Tost
 *
 */
@Component
public class Alice {
	
	// fields /////
	
	@Autowired
	private GlobalCounter games;
	
	@Autowired
	private Counter wins;

	
	// methods /////
	
	public void wins() {
		
		this.games.increase();
		this.wins.increase();
		
	}
	
	public void show() {
		
		Demo.log(
			"Alice has won %d out of %d games.",
			this.wins.getCount(),
			this.games.getCount()
		);
		
	}

}
