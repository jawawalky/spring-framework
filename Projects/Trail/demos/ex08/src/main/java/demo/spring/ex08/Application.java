/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex08;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import demo.util.Demo;

/**
 * In this demo you learn, how to autowire beans.
 * 
 * @author Franz Tost
 *
 */
@Component
public class Application {
	
	// fields /////
	
	@Autowired
	private Duke duke;
	
	@Autowired
	private Alice alice;
	

	// constructors /////

	private Application() { }
	

	// methods /////

	private void runDemo() {

		Demo.log("Running application ...");
		
		this.alice.wins();
		this.duke.wins();
		this.duke.wins();
		this.alice.wins();
		this.alice.wins();
		
		this.duke.show();
		this.alice.show();
		
		Demo.log("Finished.");

	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		try (
			AnnotationConfigApplicationContext context =
			    new AnnotationConfigApplicationContext(
			    	ApplicationConfiguration.class
			    );
		) {
			
			final Application application = context.getBean(Application.class);
			application.runDemo();
	
		} // try
			
	}

}
