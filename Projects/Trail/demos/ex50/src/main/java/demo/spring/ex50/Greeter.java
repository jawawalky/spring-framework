/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2021 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex50;

/**
 * A simple bean, which says <i>Hello</i>.
 * 
 * @author Franz Tost
 *
 */
public class Greeter {
	
	// methods /////
	
	public String saysHello() {
		
		return "Hello from demo 50";
		
	}

}
