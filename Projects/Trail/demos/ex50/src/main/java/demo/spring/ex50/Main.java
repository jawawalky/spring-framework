/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2021 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex50;

import org.apache.catalina.Context;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.startup.Tomcat;

import demo.util.Demo;

/**
 * The application starts a <i>Tomcat</i> WebServer with our Web application.
 * When the server is running, you can open a browser window and call
 * <pre>
 * 	<code>http://localhost:8080/demo/hello</code>
 * </pre>
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors /////

	private Main() { }
	

	// methods /////

	private void runServer() {

		Demo.log("Running server ...");
		
		try {
			
			final Tomcat tomcat = new Tomcat();
			 
			tomcat.setBaseDir("tomcat");
	        tomcat.setPort(8080);
	        
	        final Context context = tomcat.addWebapp("/demo", "demo");
	        Tomcat.addServlet(context, "helloServlet", new HelloServlet());      
	        context.addServletMappingDecoded("/hello", "helloServlet");
	         
	        tomcat.start();
	        tomcat.getServer().await();
			
		} // try
		catch (LifecycleException e) {
			
			Demo.log(e);
			
		} // catch
		
		Demo.log("Finished.");

	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Main().runServer();

	}

}
