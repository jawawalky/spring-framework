/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2018 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex35.service;

import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;
import java.util.function.Predicate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionTemplate;

import demo.spring.ex35.dao.ContactDAO;
import demo.spring.ex35.model.Contact;

/**
 * An address book, which is a service for the administration of
 * {@code Contact}s.
 * 
 * @author Franz Tost
 *
 */
@Component
public class AddressBook {

	// fields /////
	
	private AtomicLong idGenerator = new AtomicLong(0L);

	@Autowired
	private TransactionTemplate transactionTemplate;
	
	@Autowired
	private ContactDAO contactDAO;
	
	
	// methods /////
	
	public Long newContact(final Contact contact) {
		
		return
			this.transactionTemplate.execute(
				status -> {
					Long id = idGenerator.addAndGet(1L);
					contact.setId(id);
					contactDAO.create(contact);
					return id;
				}
			);
			
	}
	
	public Contact getContact(final Long id) {
		
		return
			this.transactionTemplate.execute(
				status -> contactDAO.read(id)
			);
		
	}
	
	public void updateContact(final Contact contact) {
		
		this.transactionTemplate.executeWithoutResult(
			status -> contactDAO.update(contact)
		);
		
	}
	
	public void removeContact(final Long id) {
		
		this.transactionTemplate.executeWithoutResult(
			status -> contactDAO.delete(id)
		);
		
	}
	
	
	public void patchContacts(
		final Predicate<? super Contact> predicate,
		final Consumer<Contact>          patch
	) {

		this.transactionTemplate.executeWithoutResult(
			status ->
				contactDAO.getAll()
					.stream()
					.filter(predicate)
					.forEach(
						contact -> {
							patch.accept(contact);
							contactDAO.update(contact);
						}
					)
		);
		
	}
	
	public void forEachContact(final Consumer<? super Contact> action) {
		
		this.contactDAO.getAll().forEach(action);
		
	}
	
}
