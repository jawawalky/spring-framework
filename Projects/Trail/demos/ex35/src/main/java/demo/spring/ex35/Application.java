/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2018 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex35;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

import demo.spring.ex35.model.Contact;
import demo.spring.ex35.service.AddressBook;
import demo.util.Demo;

/**
 * An example for using DAO objects.
 * 
 * @author Franz Tost
 *
 */
@Configuration
@Component
@ComponentScan({ "demo.spring.ex35.jdbc", "demo.spring.ex35.service" })
public class Application {

	// fields /////

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private AddressBook addressBook;
	
	
	// beans /////
	
	@Bean
	public DataSource dataSource() {
		
		try {
			
			Class.forName("org.hsqldb.jdbcDriver");
			return new DriverManagerDataSource("jdbc:hsqldb:mem:demo", "sa", null);
			
		} // try
		catch (ClassNotFoundException e) {
			
			throw new RuntimeException(e);
			
		} // catch
		
	}
	
	@Bean
	public JdbcTemplate jdbcTemplate() {
		
		return new JdbcTemplate(this.dataSource());
		
	}
	
	@Bean
	public PlatformTransactionManager transactionManager() {
		
		return new DataSourceTransactionManager(this.dataSource());
		
	}

	@Bean
	public TransactionTemplate transactionTemplate() {
		
		return new TransactionTemplate(this.transactionManager());
		
	}

	
	// methods /////

	private void runDemo() {

		Demo.log("Running demo ...");
		
		this.setupSchema();
		
		Demo.log("Creating contacts ...");
		this.createContacts();
		this.showContacts();
		
		Demo.log("Patchting contacts ...");
		this.patchContacts();
		this.showContacts();
		
		Demo.log("Finished.");

	}
	
	private void createContacts() {
		
		this.addressBook.newContact(new Contact("Hofbr\u00e4uhaus",        "Platzl",         9, "80331", "Hamburg"));
		this.addressBook.newContact(new Contact("Cl\u00e4rchens Ballhaus", "Auguststraße",  24, "10117", "Berlin"));
		this.addressBook.newContact(new Contact("Lenbachhaus",             "Luisenstrasse", 33, "80333", "Hamburg"));
		
	}
	
	private void patchContacts() {
		
		this.addressBook.patchContacts(
			contact -> "Hamburg".equals(contact.getCity()),
			contact -> contact.setCity("M\u00fcnchen")
		);
		
	}
	
	private void showContacts() {
		
		this.addressBook.forEachContact(
			contact -> Demo.log(contact.toString())
		);
		
	}
	
    private void setupSchema() {
		
        Demo.log("Creating schema ...");
		
        this.jdbcTemplate.update(
            "CREATE TABLE Addressbook (" +
            "id INTEGER, "               +
            "name VARCHAR(50), "         +
            "street VARCHAR(100), "      +
            "houseNo INTEGER, "          +
            "zipCode VARCHAR(10), "      +
            "city VARCHAR(30))"
        );
	    
	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {
		
		try (
			AnnotationConfigApplicationContext context =
			    new AnnotationConfigApplicationContext(Application.class);
		) {
			
			context.getBean(Application.class).runDemo();
	
		} // try
		
	}

}
