/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2018 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex35.dao;

import java.util.List;

import demo.spring.ex35.model.Contact;

/**
 * A DAO interface for {@code Contact}s.
 * 
 * @author Franz Tost
 *
 */
public interface ContactDAO {

	// methods
	// ........................................................................
	
	Long    create(final Contact contact);
	Contact read(final Long id);
	void    update(final Contact contact);
	void    delete(final Long id);
	
	List<Contact> getAll();

}
