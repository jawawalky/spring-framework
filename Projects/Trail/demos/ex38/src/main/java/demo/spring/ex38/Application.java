/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2018 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex38;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import demo.spring.ex38.model.Contact;
import demo.spring.ex38.service.AddressBook;
import demo.util.Demo;

/**
 * An example, which uses JPA for persistence.
 * 
 * @author Franz Tost
 *
 */
public class Application {

	// constructors /////

	private Application() { }

	
	// methods /////

	private void run() {

		Demo.log("Running demo ...");
		
		try (
			ClassPathXmlApplicationContext context =
			    new ClassPathXmlApplicationContext("beans.xml");
		) {
			
			final AddressBook addressBook = context.getBean(AddressBook.class);
	
			Demo.log("Creating contacts ...");
			this.createContacts(addressBook);
			this.showContacts(addressBook);
			
			Demo.log("Patchting contacts ...");
			this.patchContacts(addressBook);
			this.showContacts(addressBook);
			
		} // try
			
		Demo.log("Finished.");

	}
	
	private void createContacts(final AddressBook addressBook) {
		
		addressBook.newContact(new Contact("Hofbr\u00e4uhaus",        "Platzl",         9, "80331", "Hamburg"));
		addressBook.newContact(new Contact("Cl\u00e4rchens Ballhaus", "Auguststraße",  24, "10117", "Berlin"));
		addressBook.newContact(new Contact("Lenbachhaus",             "Luisenstrasse", 33, "80333", "Hamburg"));
		
	}
	
	private void patchContacts(final AddressBook addressBook) {
		
		addressBook.patchContacts(
			contact -> "Hamburg".equals(contact.getCity()),
			contact -> contact.setCity("M\u00fcnchen")
		);
		
	}
	
	private void showContacts(final AddressBook addressBook) {
		
		addressBook.forEachContact(
			contact -> Demo.log(contact.toString())
		);
		
	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {
		
		new Application().run();
		
	}

}
