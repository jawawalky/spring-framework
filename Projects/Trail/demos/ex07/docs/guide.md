# A Bean's Life-cycle

## After Creation

Normally the constructor of a *Java* class is used to initialize an object.
This can also be done with *Spring* beans. But there is a pitfall, because
dependent beans have not been injected, yet. So when we try to access
a dependency in the constructor of a bean, we will get a `null` reference,
usually resulting in a `NullPointerException`.

So a better place to do initializations in a *Spring* bean is a separate
method, which is called after the constructor has been called and dependencies
have been injected. You can write any method, which takes no parameters and
whose return type is `void` and which you annotate with `@PostConstruct`.

**Example:**

```
import javax.annotation.PostConstruct;
...

@Component
public class Translator {

  @Autowired
  private Dictionary dictionary;
  
  @PostConstruct
  private void init() {
    String translation = this.dictionary.translate("rain");
  }
  
  ...
  
}
```

## Before Destruction

The counterpart to the initialization of the bean is its destruction.
In the same way as we can define a post-construction method, we can define
a pre-destruction method. It is a method without parameters and a return type
of `void` and which is annotated by `@PreDestroy`.

Sometimes things that have been constructed in the `@PostConstruct` method
need to be destroyed in the `@PreDestroy` method.

> `PostConstruct` and `PreDestroy` are taken from the standard *JavaEE* package
> `javax.annotation` and were not defined by *Spring*.
