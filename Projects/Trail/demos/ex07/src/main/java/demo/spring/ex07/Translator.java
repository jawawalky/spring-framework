/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex07;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import demo.util.Demo;

/**
 * A translator object, which autowires a {@link Dictionary} for
 * the translations.
 * 
 * @author Franz Tost
 *
 */
@Component
public class Translator {
	
	// fields /////
	
	@Autowired
	private Dictionary dictionary;

	private Map<String, String> cache = new HashMap<String, String>();
	
	
	// methods /////
	
	@PostConstruct
	private void init() {
		
		this.addToCache("rain");
		this.addToCache("sun");
		
	}
	
	private void addToCache(final String word) {
		
		this.cache.put(word, this.dictionary.translate(word));
		
	}
	
	@PreDestroy
	private void exit() {
		
		this.cache.clear();
		
	}
	
	public void translate(final String word) {
		
		String translation = this.cache.get(word);
		
		translation =
			(translation != null) ?
			translation           :
			this.dictionary.translate(word);
		
		Demo.log(word + " --> " + translation);
		
	}

}
