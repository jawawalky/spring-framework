/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2018 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex32;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Component;

import demo.util.Demo;

/**
 * An example for using SQL objects.
 * 
 * @author Franz Tost
 *
 */
@Configuration
@Component
@ComponentScan("demo.spring.ex32")
public class Application {

	// fields /////

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private ContactCreator contactCreator;

	@Autowired
	private ContactReader contactReader;

	@Autowired
	private ContactUpdater contactUpdater;

	@Autowired
	private ContactDeleter contactDeleter;

	
	// beans /////
	
	@Bean
	public DataSource dataSource() {
		
		try {
			
			Class.forName("org.hsqldb.jdbcDriver");
			return new DriverManagerDataSource("jdbc:hsqldb:mem:demo", "sa", null);
			
		} // try
		catch (ClassNotFoundException e) {
			
			throw new RuntimeException(e);
			
		} // catch
		
	}
	
	@Bean
	public JdbcTemplate jdbcTemplate() {
		
		return new JdbcTemplate(this.dataSource());
		
	}

	
	// methods /////

	private void runDemo() {

		Demo.log("Running demo ...");
		
		this.setupSchema();
		
		final Long id = 2L;
		
		Contact contact =
			new Contact(id, "Prime Minister", "Downing Street", 11, "SW1A 2AA", "London");
		
		Demo.log("Creating contact ...");
		this.contactCreator.create(contact);
		
		Demo.log("Reading contact ...");
		contact = this.contactReader.read(id);
		Demo.log("Contact: %s", contact.toString());
		
		Demo.log("Updating contact ...");
		contact.setHouseNo(10);
		this.contactUpdater.update(contact);

		Demo.log("Reading contact ...");
		contact = this.contactReader.read(id);
		Demo.log("Contact: %s", contact.toString());

		Demo.log("Deleting contact ...");
		this.contactDeleter.delete(id);
		
		Demo.log("Reading contact ...");
		contact = this.contactReader.read(id);
		Demo.log("Contact deleted: %s", (contact == null));

		Demo.log("Finished.");

	}
	
    private void setupSchema() {
		
        Demo.log("Creating schema ...");
		
        this.jdbcTemplate.update(
            "CREATE TABLE Addressbook (" +
            "id INTEGER, "               +
            "name VARCHAR(50), "         +
            "street VARCHAR(100), "      +
            "houseNo INTEGER, "          +
            "zipCode VARCHAR(10), "      +
            "city VARCHAR(30))"
        );
	    
	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {
		
		try (
			AnnotationConfigApplicationContext context =
			    new AnnotationConfigApplicationContext(Application.class);
		) {
			
			context.getBean(Application.class).runDemo();
	
		} // try
		
	}

}
