/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex31;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

/**
 * A row mapper maps the values of a result set row to a JavaBean.
 * 
 * @author Franz Tost
 *
 */
public class ContactRowMapper implements RowMapper<Contact> {

	// methods
	// ........................................................................

	@Override public Contact mapRow(
		final ResultSet resultSet,
		final int       rowIndex
	) throws SQLException {

		return new Contact(
			resultSet.getLong("id"),
			resultSet.getString("name"),
			resultSet.getString("street"),
			resultSet.getInt("houseNo"),
			resultSet.getString("zipCode"),
			resultSet.getString("city")
		);

	}

}
