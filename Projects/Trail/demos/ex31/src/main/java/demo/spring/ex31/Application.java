/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2018 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex31;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Component;

import demo.util.Demo;

/**
 * An example for using a {@code RowMapper}.
 * 
 * @author Franz Tost
 *
 */
@Configuration
@Component
public class Application {

	// fields /////

	@Autowired
	private JdbcTemplate jdbcTemplate;

	
	// beans /////
	
	@Bean
	public DataSource dataSource() {
		
		try {
			
			Class.forName("org.hsqldb.jdbcDriver");
			return new DriverManagerDataSource("jdbc:hsqldb:mem:demo", "sa", null);
			
		} // try
		catch (ClassNotFoundException e) {
			
			throw new RuntimeException(e);
			
		} // catch
		
	}
	
	@Bean
	public JdbcTemplate jdbcTemplate() {
		
		return new JdbcTemplate(this.dataSource());
		
	}

	
	// methods /////

	private void runDemo() {

		Demo.log("Running demo ...");
		
		this.setupSchema();
		
		this.createContact();
		this.readContact();
		this.updateContact();
		this.readContact();
		this.deleteContact();
		this.readContact();
		
		Demo.log("Finished.");

	}
	
    private void setupSchema() {
		
        Demo.log("Creating schema ...");
		
        this.jdbcTemplate.update(
            "CREATE TABLE Addressbook (" +
            "id INTEGER, "               +
            "name VARCHAR(50), "         +
            "street VARCHAR(100), "      +
            "houseNo INTEGER, "          +
            "zipCode VARCHAR(10), "      +
            "city VARCHAR(30))"
        );
	    
	}
	
    private void createContact() {
		
        Demo.log("Creating contact ...");
        
        this.jdbcTemplate.update(
            "INSERT INTO Addressbook VALUES (?, ?, ?, ?, ?, ?)",
            2L,
            "Prime Minister",
            "Downing Street",
            11,
            "SW1A 2AA",
            "London"
        );

	}

	private void readContact() {
		
		Demo.log("Reading contact ...");
		
		try {
			
			ContactRowMapper rowMapper = new ContactRowMapper();
			
			Contact contact =
				this.jdbcTemplate.queryForObject(
					"SELECT * FROM Addressbook WHERE ID = ?",
					rowMapper,
					2L
				);
			
			Demo.log("Contact: %s", contact.toString());
			
		} // try
		catch (EmptyResultDataAccessException e) {
			
			Demo.log("---");
			return;
			
		} // catch
		
	}

    private void updateContact() {
		
        Demo.log("Updating contact ...");
        
        this.jdbcTemplate.update(
        	"UPDATE Addressbook SET houseNo = ? WHERE id = ?",
        	10,
        	2L
        );

	}

    private void deleteContact() {
    	
        Demo.log("Deleting contact ...");
        
        this.jdbcTemplate.update("DELETE FROM Addressbook WHERE id = ?", 2L);

	}

	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {
		
		try (
			AnnotationConfigApplicationContext context =
			    new AnnotationConfigApplicationContext(Application.class);
		) {
			
			context.getBean(Application.class).runDemo();
	
		} // try
		
	}

}
