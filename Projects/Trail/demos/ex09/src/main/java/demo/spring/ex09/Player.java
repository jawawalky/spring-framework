/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex09;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import demo.util.Demo;

/**
 * A player.
 * 
 * @author Franz Tost
 *
 */
public class Player {
	
	// fields /////
	
	private String name;
	
	@Autowired
	@Qualifier("games")
	private Counter games;
	
	@Autowired
	@Qualifier("wins")
	private Counter wins;

	
	// methods /////
	
	public void setName(final String name) {
		
		this.name = name;
		
	}
	
	public void wins() {
		
		this.games.increase();
		this.wins.increase();
		
	}
	
	public void show() {
		
		Demo.log(
			"%s has won %d out of %d games.",
			this.name,
			this.wins.getCount(),
			this.games.getCount()
		);
		
	}

}
