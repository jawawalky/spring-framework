/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex09;

/**
 * A counter.
 * 
 * @author Franz Tost
 *
 */
public class Counter {
	
	// fields /////
	
	private int count;
	
	
	// methods /////
	
	public void increase() {
		
		this.count++;
		
	}
	
	public int getCount() {
		
		return this.count;
		
	}

}
