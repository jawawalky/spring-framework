/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2018 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex39;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.rmi.RmiServiceExporter;

import demo.spring.ex39.service.AddressBook;

/**
 * The exporter configuration.
 * 
 * @author Franz Tost
 *
 */
@SuppressWarnings("deprecation")
@Configuration
public class ExporterConfig {
	
	// fields /////
	
	@Autowired
	private AddressBook addressBook;
	
	
	// beans /////

	@Bean
	public RmiServiceExporter rmiServiceExporter() {
		
		RmiServiceExporter exporter = new RmiServiceExporter();
		
		exporter.setServiceName("AddressBook");
		exporter.setService(this.addressBook);
		exporter.setServiceInterface(AddressBook.class);
		exporter.setRegistryPort(1199);
		
		return exporter;
		
	}

}
