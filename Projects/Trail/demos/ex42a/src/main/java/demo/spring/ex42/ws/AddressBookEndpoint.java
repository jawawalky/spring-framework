/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2018 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex42.ws;

import javax.jws.WebMethod;
import javax.jws.WebService;

import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import demo.spring.ex42.model.AddressBook;
import demo.spring.ex42.model.Contact;

/**
 * A DAO-based address book implementation.
 * 
 * @author Franz Tost
 *
 */
@WebService(serviceName="AddressBookEndpoint")
public class AddressBookEndpoint
	extends
		SpringBeanAutowiringSupport
	implements
		AddressBook
{

	// fields
	// ........................................................................

	private AddressBook addressBook;
	
	// constructors
	// ........................................................................
	
	public AddressBookEndpoint(final AddressBook addressBook) {
		
		super();
		
		this.addressBook = addressBook;
		
	}
	
	// methods
	// ........................................................................
	
	@WebMethod
	@Override public Long newContact(final Contact contact) {
		
		return this.addressBook.newContact(contact);
		
	}
	
	@WebMethod
	@Override public Contact getContact(final Long id) {
		
		return addressBook.getContact(id);
		
	}
	
	@WebMethod
	@Override public void updateContact(final Contact contact) {
		
		this.addressBook.updateContact(contact);
		
	}
	
	@WebMethod
	@Override public void removeContact(final Long id) {
		
		this.addressBook.removeContact(id);
		
	}
	
	
}
