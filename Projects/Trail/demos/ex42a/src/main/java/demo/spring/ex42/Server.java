/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2018 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex42;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import demo.util.Demo;

/**
 * An example for a RMI server.
 * 
 * @author Franz Tost
 *
 */
@Configuration
@Component
public class Server {

	// fields
	// ........................................................................
	
	// constructors
	// ........................................................................

	private Server() { }

	// methods
	// ........................................................................

	private void runDemo() {

		Demo.log("Running demo ...");
		
		try (
			ClassPathXmlApplicationContext context =
			    new ClassPathXmlApplicationContext("beans.xml");
		) {
			
			Demo.log("Server is running ...");
			Demo.input("Press any key to shut down server");
			
		} // try
		catch (Exception e) {
			
			Demo.terminate(e, -1);
			
		} // catch
			
		Demo.log("Finished.");

	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {
		
		try (
			AnnotationConfigApplicationContext context =
			    new AnnotationConfigApplicationContext(Server.class);
		) {
			
			context.getBean(Server.class).runDemo();
	
		} // try
		
	}

}
