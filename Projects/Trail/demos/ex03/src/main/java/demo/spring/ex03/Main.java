/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex03;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import demo.util.Demo;

/**
 * This demo teaches you, how you can define beans by annotating classes
 * with certain annotations such as {@code Component}.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors /////

	private Main() { }
	

	// methods /////

	private void runDemo() {

		Demo.log("Running demo ...");
		
		try (
			ClassPathXmlApplicationContext context =
			    new ClassPathXmlApplicationContext("beans.xml");
		) {
	
			final Dictionary dictionary =
				context.getBean(Dictionary.class);    // <- We use the implemented
                                                      //    interface 'Dictionary'
                                                      //    as key, to find the bean
                                                      //    in the context.

			this.printTranslation(dictionary, "rain");
			this.printTranslation(dictionary, "sun");
			this.printTranslation(dictionary, "snow");
			this.printTranslation(dictionary, "wind");
		
		} // try
		
		Demo.log("Finished.");

	}
	
	private void printTranslation(
		final Dictionary dictionary,
		final String     wordInEnglish
	) {
		
		Demo.log(wordInEnglish + " --> " + dictionary.translate(wordInEnglish));
		
	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Main().runDemo();

	}

}
