/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex03;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

/**
 * A dictionary, which translates English words into German words.
 * 
 * @author Franz Tost
 *
 */
@Component                        // <- That marks the German dictionary as
public class GermanDictionary     //    a component that will be bound in
                                  //    the application context as a bean.

	implements Dictionary         // <- The dictionary will be bound under
                                  //    several keys in the context.
                                  //    one is the implemented interface
                                  //    'Dictionary'.
{
	
	// fields /////
	
	private Map<String, String> words = new HashMap<String, String>();
	
	
	// constructor /////
	
	public GermanDictionary() {
		
		super();
		
		this.words.put("rain", "Regen");
		this.words.put("wind", "Wind");
		this.words.put("sun",  "Sonne");
		this.words.put("snow", "Schnee");
		
	}

	
	// methods /////

	@Override public String translate(final String word) {
		
		return this.words.get(word);
		
	}

}
