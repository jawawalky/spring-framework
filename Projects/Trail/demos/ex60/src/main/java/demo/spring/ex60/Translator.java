/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex60;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * A translator bean.
 * 
 * @author Franz Tost
 *
 */
@Component
public class Translator {
	
	// fields /////
	
	@Autowired
	private Dictionary dictionary;

	
	// methods /////
	
	public String translate(final String word) {
		
		return this.dictionary.translate(word);
		
	}

}
