/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2021 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex60;

import org.springframework.stereotype.Component;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRegistration;

/**
 * An {@code WebApplicationInitializer} for this demo application.
 * 
 * @author Franz Tost
 *
 */
@Component
public class DemoWebApplicationInitializer implements WebApplicationInitializer {
	
	// methods /////
	
	@Override
	public void onStartup(
		final ServletContext servletContext
	) throws ServletException {
		
		final AnnotationConfigWebApplicationContext context =
			new AnnotationConfigWebApplicationContext();
		
		context.scan("demo.spring.ex60");
		context.refresh();
		
		servletContext.addListener(new ContextLoaderListener(context));
		servletContext.addListener(new RequestContextListener());
		
		final ServletRegistration.Dynamic translatorServlet =
			servletContext.addServlet("translator", new DispatcherServlet());
		translatorServlet.setLoadOnStartup(1);
		translatorServlet.addMapping("/translate");
	
	}

}
