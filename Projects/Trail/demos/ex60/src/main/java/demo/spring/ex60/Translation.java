/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex60;

import static org.springframework.web.context.WebApplicationContext.SCOPE_REQUEST;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * A translation.
 * 
 * @author Franz Tost
 *
 */
@Component
@Scope(SCOPE_REQUEST)
public class Translation {
	
	// fields /////
	
	private String word;
	
	private String translation;
	
	
	// methods /////
	
	public String getWord()            { return this.word; }
	public void   setWord(String word) { this.word = word; }
	
	public String getTranslation()                   { return this.translation;        }
	public void   setTranslation(String translation) { this.translation = translation; }

}
