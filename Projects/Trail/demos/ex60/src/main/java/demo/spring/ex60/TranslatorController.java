/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex60;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.servlet.ModelAndView;

/**
 * A controller for a translator UI.
 * 
 * @author Franz Tost
 *
 */
@Controller
@RequestMapping("/translate")
public class TranslatorController {

	// constants /////
	
	private static String VIEW = "translate";
	
	
	// fields /////
	
	@Autowired
	private Translator translator;

	
	// methods /////
	
	@RequestMapping(method = GET)
	public ModelAndView onGet() {
		
		return new ModelAndView(VIEW);
		
	}
	
	@RequestMapping(method = POST)
	public ModelAndView onPost(@RequestParam final String word) {
		
		final ApplicationContext ctx =
			ContextLoader.getCurrentWebApplicationContext();
		
		final Translation translation = ctx.getBean(Translation.class);
		translation.setWord(word);
		translation.setTranslation(this.translator.translate(word));
		
		return new ModelAndView(VIEW, "translation", translation);
		
	}
	
}
