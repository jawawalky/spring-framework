<%@ page pageEncoding='UTF-8' %>

<html>
	<head>
		<title>Translator</title>
	</head>
	<body>
		<h1>Translator</h1>
		
		<form method='post'>
			<input
				name  = 'word'
				type  = 'text'
				value = '${translation.word}' />
			&nbsp;
			--&gt;
			&nbsp;
			<input
				name  = 'translation'
				type  = 'text'
				value = '${translation.translation}' />
			&nbsp;
			<input type='submit' value='Translate' />
		</form>
	</body>
</html>
