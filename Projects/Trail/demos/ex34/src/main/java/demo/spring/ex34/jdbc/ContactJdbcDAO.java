/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2018 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex34.jdbc;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import demo.spring.ex34.dao.ContactDAO;
import demo.spring.ex34.model.Contact;

/**
 * A DAO interface for {@code Contact}s.
 * 
 * @author Franz Tost
 *
 */
@Component
public class ContactJdbcDAO implements ContactDAO {

	// fields /////

	@Autowired
	private ContactCreator contactCreator;
	
	@Autowired
	private ContactReader contactReader;
	
	@Autowired
	private ContactUpdater contactUpdater;
	
	@Autowired
	private ContactDeleter contactDeleter;
	
	@Autowired
	private AllContacts allContacts;

	
	// methods /////
	
	@Override public Long create(final Contact contact) {
		
		this.contactCreator.create(contact);
		return contact.getId();
		
	}

	@Override public Contact read(final Long id) {
		
		return this.contactReader.read(id);
		
	}

	@Override public void update(final Contact contact) {
		
		this.contactUpdater.update(contact);
		
	}

	@Override public void delete(final Long id) {
		
		this.contactDeleter.delete(id);
		
	}

	@Override public List<Contact> getAll() {
		
		return this.allContacts.execute();
		
	}

}
