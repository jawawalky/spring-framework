/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2018 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex34.service;

import static org.springframework.transaction.TransactionDefinition.PROPAGATION_REQUIRED;
import static org.springframework.transaction.TransactionDefinition.PROPAGATION_REQUIRES_NEW;

import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;
import java.util.function.Predicate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import demo.spring.ex34.dao.ContactDAO;
import demo.spring.ex34.model.Contact;

/**
 * An address book, which is a service for the administration of
 * {@code Contact}s.
 * 
 * @author Franz Tost
 *
 */
@Component
public class AddressBook {

	// fields /////
	
	private AtomicLong idGenerator = new AtomicLong(0L);

	@Autowired
	private PlatformTransactionManager transactionManager;
	
	@Autowired
	private ContactDAO contactDAO;
	
	
	// methods /////
	
	public Long newContact(final Contact contact) {
		
		TransactionStatus transactionStatus =
			this.beginTransaction(PROPAGATION_REQUIRED);
		
		try {
			
			Long id = this.idGenerator.addAndGet(1L);
			contact.setId(id);
			this.contactDAO.create(contact);
			
			this.transactionManager.commit(transactionStatus);
			
			return id;
			
		} // try
		catch (RuntimeException e) {
			
			this.transactionManager.rollback(transactionStatus);
			throw e;
			
		} // catch
		
	}
	
	public Contact getContact(final Long id) {
		
		TransactionStatus transactionStatus =
			this.beginTransaction(PROPAGATION_REQUIRED);
		
		try {
				
			Contact contact = this.contactDAO.read(id);
			this.transactionManager.commit(transactionStatus);
			return contact;
			
		} // try
		catch (RuntimeException e) {
			
			this.transactionManager.rollback(transactionStatus);
			throw e;
			
		} // catch
		
	}
	
	public void updateContact(final Contact contact) {
		
		TransactionStatus transactionStatus =
			this.beginTransaction(PROPAGATION_REQUIRED);
		
		try {
				
			this.contactDAO.update(contact);
			this.transactionManager.commit(transactionStatus);
			
		} // try
		catch (RuntimeException e) {
			
			this.transactionManager.rollback(transactionStatus);
			throw e;
			
		} // catch
		
	}
	
	public void removeContact(final Long id) {
		
		TransactionStatus transactionStatus =
			this.beginTransaction(PROPAGATION_REQUIRED);
		
		try {
				
			this.contactDAO.delete(id);
			this.transactionManager.commit(transactionStatus);
			
		} // try
		catch (RuntimeException e) {
			
			this.transactionManager.rollback(transactionStatus);
			throw e;
			
		} // catch
		
	}
	
	
	public void patchContacts(
		final Predicate<? super Contact> predicate,
		final Consumer<Contact>          patch
	) {
		
		TransactionStatus transactionStatus =
			this.beginTransaction(PROPAGATION_REQUIRES_NEW);
		
		try {
			
			this.contactDAO.getAll().stream().filter(predicate).forEach(
					
				contact -> {
					
					patch.accept(contact);
					this.contactDAO.update(contact);
					
				}
				
			);
			
			this.transactionManager.commit(transactionStatus);
			
		} // try
		catch (RuntimeException e) {
			
			this.transactionManager.rollback(transactionStatus);
			throw e;
			
		} // catch
				
	}
	
	public void forEachContact(final Consumer<? super Contact> action) {
		
		this.contactDAO.getAll().forEach(action);
		
	}
	
	private TransactionStatus beginTransaction(final int propagation) {
		
		DefaultTransactionDefinition transactionDefinition =
			new DefaultTransactionDefinition();
		transactionDefinition.setPropagationBehavior(propagation);
		return this.transactionManager.getTransaction(transactionDefinition);
		
	}

}
